<?php $options = _WSH()->option();
	get_header(); 
	$settings  = oviedo_set(oviedo_set(get_post_meta(get_the_ID(), 'bunch_page_meta', true) , 'bunch_page_options') , 0);
	$meta = _WSH()->get_meta('_bunch_layout_settings');
	$meta1 = _WSH()->get_meta('_bunch_header_settings');
	$meta2 = _WSH()->get_meta();
	_WSH()->page_settings = $meta;
	if(oviedo_set($_GET, 'layout_style')) $layout = oviedo_set($_GET, 'layout_style'); else
	$layout = oviedo_set( $meta, 'layout', 'right' );
	if( !$layout || $layout == 'full' || oviedo_set($_GET, 'layout_style')=='full' ) $sidebar = ''; else
	$sidebar = oviedo_set( $meta, 'sidebar', 'default-sidebar' );
	
	$sidebar = ( $sidebar ) ? $sidebar : 'default-sidebar';
	$layout = ( $layout ) ? $layout : 'right';
	
	$classes = ( !$layout || $layout == 'full' || oviedo_set($_GET, 'layout_style')=='full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12 ' : ' col-lg-9 col-md-8 col-sm-12 col-xs-12 ' ;
	_WSH()->post_views( true );
	$bg = oviedo_set($meta1, 'header_img');
	$title = oviedo_set($meta1, 'header_title');
?>

<!--Page Title-->
<section class="page-title" <?php if($bg):?>style="background-image:url(<?php echo esc_url($bg)?>);"<?php endif;?>>
    <div class="auto-container">
        <h1><?php if($title) echo wp_kses_post($title); else wp_title('');?></h1>
        <?php echo wp_kses_post(oviedo_get_the_breadcrumb()); ?>
    </div>
</section>
<!--End Page Title-->

<!--Blog Single Section-->
<section class="blog-single-section">
    <div class="auto-container">
    	<div class="row">
            <!-- sidebar area -->
            <?php if( $layout == 'left' ): ?>
                <?php if ( is_active_sidebar( $sidebar ) ) { ?>
                    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">        
                        <aside class="sidebar">
                            <?php dynamic_sidebar( $sidebar ); ?>
                        </aside>
                    </div>
                <?php } ?>
            <?php endif; ?>
    
            <!--Content Side-->	
            <div class="<?php echo esc_attr($classes);?>">
                <div class="thm-unit-test">
                <?php while( have_posts() ): the_post();
                    global $post; 
                    $post_meta = _WSH()->get_meta();
                ?>
                    <!--Blog Detail-->
        
                    <div class="blog-detail">
                        <div class="inner-box">
                            
                            <?php the_content();?>
                            <div class="clearfix"></div>
                            <?php wp_link_pages(array('before'=>'<div class="paginate-links">'.esc_html__('Pages: ', 'oviedo'), 'after' => '</div>', 'link_before'=>'<span>', 'link_after'=>'</span>')); ?>
                            
                            <!--post-share-options-->
                            <div class="post-share-options clearfix">
                                <div class="pull-left tags"><?php the_tags('<span> Tags: </span>', ', ');?></div>
                                <?php if(function_exists('bunch_share_us')) echo wp_kses_post(bunch_share_us(get_the_id(),$post->post_name ));?>
                            </div>
                            
                            <!--Posts Nav-->
                            <div class="posts-nav">
                                <div class="clearfix">
                                    <div class="pull-left">
                                        <?php previous_post_link('%link','<div class="prev-post"><span class="fa fa-long-arrow-left"></span> Prev Post</div>'); ?>
                                    </div>
                                    <div class="pull-right">
                                        <?php next_post_link('%link','<div class="next-post">Next Post <span class="fa fa-long-arrow-right"></span> </div>'); ?>
                                    </div>                                
                                </div>
                            </div>
                            
                        </div>
                    </div>
                
                <?php if(get_the_author_meta('description')):?>
                <!--Author Box-->
                <div class="blog-author-box row">
                    <div class="auto-container">
                        <div class="author-inner">
                            <div class="image">
                                <?php echo get_avatar('', 175 ); ?>
                            </div>
                            <h3><?php the_author(); ?></h3>
                            <div class="text"><?php the_author_meta( 'description', get_the_author_meta('ID') );?></div>
                        </div>
                    </div>
                </div>
                <!--End Author Box-->
                <?php endif; ?>
                 
                <!-- comment area -->
                
                <?php comments_template(); ?><!-- end comments -->    
            
                <?php endwhile;?>
                </div>
            </div>
            <!--Content Side-->
            
            <!-- sidebar area -->
            <?php if( $layout == 'right' ): ?>
                <?php if ( is_active_sidebar( $sidebar ) ) { ?>
                    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">        
                        <aside class="sidebar">
                            <?php dynamic_sidebar( $sidebar ); ?>
                        </aside>
                    </div>
                <?php } ?>
            <?php endif; ?>
		</div>  
	</div> 
</section>

<?php get_footer(); ?>