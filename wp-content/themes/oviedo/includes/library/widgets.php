<?php

///----footer widgets---
//About Us
class Bunch_About_us extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_About_us', /* Name */esc_html__('Oviedo About Us','oviedo'), array( 'description' => esc_html__('Show the information about company', 'oviedo' )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		echo wp_kses_post($before_widget);?>
      		
			<div class="logo-widget">
                <div class="widget-content">
                    <div class="logo-box">
                        <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo esc_url($instance['logo_image']); ?>" alt="" /></a>
                    </div>
                    <div class="text"><?php echo wp_kses_post($instance['content']); ?></div>
                    <div class="email"><?php echo sanitize_email($instance['email']); ?></div>
                    <div class="number"><?php echo wp_kses_post($instance['phone']); ?></div>
                </div>
            </div>
            
		<?php
		
		echo wp_kses_post($after_widget);
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['logo_image'] = $new_instance['logo_image'];
		$instance['content'] = $new_instance['content'];
		$instance['email'] = $new_instance['email'];
		$instance['phone'] = $new_instance['phone'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$logo_image = ($instance) ? esc_attr($instance['logo_image']) : 'http://tonatheme.com/newwp/oviedo/wp-content/uploads/2017/10/footer-logo-3.png';
		$content = ($instance) ? esc_attr($instance['content']) : '';
		$email = ($instance) ? esc_attr($instance['email']) : '';
		$phone = ( $instance ) ? esc_attr($instance['phone']) : '';?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('logo_image')); ?>"><?php esc_html_e('Footer Logo Image:', 'oviedo'); ?></label>
            <input placeholder="" class="widefat" id="<?php echo esc_attr($this->get_field_id('logo_image')); ?>" name="<?php echo esc_attr($this->get_field_name('logo_image')); ?>" type="text" value="<?php echo esc_attr($logo_image); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('content')); ?>"><?php esc_html_e('Content:', 'oviedo'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('content')); ?>" name="<?php echo esc_attr($this->get_field_name('content')); ?>" ><?php echo wp_kses_post($content); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('email')); ?>"><?php esc_html_e('Email:', 'oviedo'); ?></label>
            <input placeholder="<?php esc_html_e('oviedo@gmail.com', 'oviedo');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('email')); ?>" name="<?php echo esc_attr($this->get_field_name('email')); ?>" type="text" value="<?php echo esc_attr($email); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('phone')); ?>"><?php esc_html_e('Phone Number:', 'oviedo'); ?></label>
            <input placeholder="<?php esc_html_e('+202-277-3894', 'oviedo');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('phone')); ?>" name="<?php echo esc_attr($this->get_field_name('phone')); ?>" type="text" value="<?php echo esc_attr($phone); ?>" />
        </p>        
                
		<?php 
	}
	
}


///----footer widgets---
//Subscribe Us
class Bunch_Subscribe_Us extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'Bunch_Subscribe_Us', /* Name */esc_html__('Oviedo Subscribe Us','oviedo'), array( 'description' => esc_html__('Show the Subscribe Us', 'oviedo' )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo wp_kses_post($before_widget);?>
      		
			<!--Footer Column-->
            <div class="subscribe-widget">
                <?php echo wp_kses_post($before_title.$title.$after_title); ?>
                <div class="widget-content">
                    <!--Social Icon Two-->
                    <?php if( $instance['show'] ): ?>
                    <ul class="social-icon-two">
                        <?php echo wp_kses_post(oviedo_get_social_icons()); ?>
                    </ul>
                    <?php endif; ?>
                    
                    <h2><?php echo wp_kses_post($instance['form_title']); ?></h2>
                    <div class="text"><?php echo wp_kses_post($instance['form_text']); ?></div>
                    <div class="email-form">
                        <form method="get" action="http://feedburner.google.com/fb/a/mailverify" accept-charset="utf-8">
                            <div class="form-group">
                                <input type="hidden" id="uri2" name="uri" value="<?php echo wp_kses_post($instance['form_id']); ?>">
                                <input type="email" name="email" value="" placeholder="<?php esc_html_e('Your e-mail', 'oviedo'); ?>" required>
                                <button type="submit"><span class="icon fa fa-angle-right"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
		<?php
		
		echo wp_kses_post($after_widget);
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['show'] = $new_instance['show'];
		$instance['form_title'] = $new_instance['form_title'];
		$instance['form_text'] = $new_instance['form_text'];
		$instance['form_id'] = $new_instance['form_id'];
		

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : 'Follow Us';
		$show = ( $instance ) ? esc_attr($instance['show']) : '';
		$form_title = ($instance) ? esc_attr($instance['form_title']) : '';
		$form_text = ( $instance ) ? esc_attr($instance['form_text']) : '';
		$form_id = ( $instance ) ? esc_attr($instance['form_id']) : '';?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'oviedo'); ?></label>
            <input placeholder="<?php esc_html_e('Follow Us', 'oviedo');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('show')); ?>"><?php esc_html_e('Show Social Icons:', 'oviedo'); ?></label>
			<?php $selected = ( $show ) ? ' checked="checked"' : ''; ?>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('show')); ?>"<?php echo esc_attr($selected); ?> name="<?php echo esc_attr($this->get_field_name('show')); ?>" type="checkbox" value="true" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('form_title')); ?>"><?php esc_html_e('Form Title:', 'oviedo'); ?></label>
            <input placeholder="<?php esc_html_e('Subscribe Us', 'oviedo');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('form_title')); ?>" name="<?php echo esc_attr($this->get_field_name('form_title')); ?>" type="text" value="<?php echo esc_attr($form_title); ?>" />
        </p> 
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('form_text')); ?>"><?php esc_html_e('Content:', 'oviedo'); ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('form_text')); ?>" name="<?php echo esc_attr($this->get_field_name('form_text')); ?>" ><?php echo wp_kses_post($form_text); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('form_id')); ?>"><?php esc_html_e('FeedBurner ID:', 'oviedo'); ?></label>
            <input placeholder="<?php esc_html_e('themeforest', 'oviedo');?>" class="widefat" id="<?php echo esc_attr($this->get_field_id('form_id')); ?>" name="<?php echo esc_attr($this->get_field_name('form_id')); ?>" type="text" value="<?php echo esc_attr($form_id); ?>" />
        </p>      
                
		<?php 
	}
	
}