<?php $options = _WSH()->option();
	oviedo_bunch_global_variable();
	$extra_class = oviedo_set($options, 'header_class');
?>
 	
    <!-- Main Header-->
    <header class="main-header header-style-five <?php if($extra_class): ?>contact-page-header<?php endif;?>">
    
    	<!-- Main Box -->
    	<div class="main-box">
        	
            <div class="outer-container clearfix">
                <!--Logo Box-->
                <div class="logo-box">
                    <?php if(oviedo_set($options, 'logo_image_5')):?>
                        <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(oviedo_set($options, 'logo_image_5'));?>" alt="<?php esc_html_e('Oviedo', 'oviedo');?>" title="<?php esc_html_e('Oviedo', 'oviedo');?>"></a></div>
                    <?php else:?>
                        <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_template_directory_uri().'/images/logo-3.png');?>" alt="<?php esc_html_e('Oviedo', 'oviedo');?>"></a></div>
                    <?php endif;?>
                </div>
                
                <?php if(oviedo_set($options, 'button')): ?>
                <!--Btn Outer-->
                <div class="btn-outer">
                    <a href="<?php echo esc_url(oviedo_set($options, 'btn_link')); ?>" class="theme-btn donate-btn btn-style-ten"><?php echo wp_kses_post(oviedo_set($options, 'btn_title')); ?></a>
                </div>
                <?php endif; ?>
                
                <!--Nav Outer-->
                <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_id' => 'navbar-collapse-1',
									'container_class'=>'navbar-collapse collapse navbar-right',
									'menu_class'=>'nav navbar-nav',
									'fallback_cb'=>false, 
									'items_wrap' => '%3$s', 
									'container'=>false,
									'walker'=> new Bunch_Bootstrap_walker()  
								) ); ?>
                             </ul>
                        </div>
                    </nav>
                    <!-- Main Menu End-->
                    
                    <!-- Main Menu End-->
                    <?php if(has_nav_menu('main_menu')):?>
                    <div class="outer-box">
                    	<!--Search Box-->
                        <div class="search-box-outer">
                            <div class="dropdown">
                                <button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-search"></span></button>
                                <ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu3">
                                    <li class="panel-outer">
                                        <div class="form-container">
                                            <?php get_template_part('searchform3')?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <?php endif;?>
                    
                </div>
                <!--Nav Outer End-->
                
            </div>    
            
        </div>
        
        <!--Sticky Header-->
        <div class="sticky-header">
        	<div class="auto-container clearfix">
            	<!--Logo-->
            	<div class="logo pull-left">
                	<?php if(oviedo_set($options, 'responsive_logo_3')):?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="img-responsive"><img src="<?php echo esc_url(oviedo_set($options, 'responsive_logo_3'));?>" alt="<?php esc_html_e('Oviedo', 'oviedo');?>" title="<?php esc_html_e('Oviedo', 'oviedo');?>"></a>
                    <?php else:?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="img-responsive"><img src="<?php echo esc_url(get_template_directory_uri().'/images/logo-small-1.png');?>" alt="<?php esc_html_e('Oviedo', 'oviedo');?>"></a>
                    <?php endif;?>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                	<!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_id' => 'navbar-collapse-1',
									'container_class'=>'navbar-collapse collapse navbar-right',
									'menu_class'=>'nav navbar-nav',
									'fallback_cb'=>false, 
									'items_wrap' => '%3$s', 
									'container'=>false,
									'walker'=> new Bunch_Bootstrap_walker()  
								) ); ?>
                             </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
                
            </div>
        </div>
        <!--End Sticky Header-->
    
    </header>
    <!--End Main Header -->