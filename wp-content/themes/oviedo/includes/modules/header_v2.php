<?php $options = _WSH()->option();
	oviedo_bunch_global_variable();
?>
 	
    <!-- Main Header / Header Style Two-->
    <header class="main-header header-style-two">
    	
        <!--Header-Upper-->
        <div class="header-upper">
        	<div class="auto-container">
            	<div class="clearfix">
                	
                	<div class="pull-left logo-outer">
                    	<?php if(oviedo_set($options, 'logo_image_2')):?>
                            <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(oviedo_set($options, 'logo_image_2'));?>" alt="<?php esc_html_e('Oviedo', 'oviedo');?>" title="<?php esc_html_e('Oviedo', 'oviedo');?>"></a></div>
                        <?php else:?>
                            <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_template_directory_uri().'/images/logo-2.png');?>" alt="<?php esc_html_e('Oviedo', 'oviedo');?>"></a></div>
                        <?php endif;?>
                    </div>
                    
                    <div class="pull-right upper-right clearfix">
                    	
						<?php if(oviedo_set($options, 'address')):?>
                        <!--Info Box-->
                        <div class="info-box">
                        	<div class="inner-box">
                                <div class="icon-box"><span class="flaticon-maps-and-flags"></span></div>
                                <ul>
                                    <?php echo wp_kses_post(oviedo_set($options, 'address')); ?>
                                </ul>
                            </div>
                        </div>
                        <?php endif; ?>
                        
						<?php if(oviedo_set($options, 'email')):?>
                        <!--Info Box-->
                        <div class="info-box">
                        	<div class="inner-box">
                                <div class="icon-box"><span class="flaticon-24-hours"></span></div>
                                <ul>
                                	<li><strong><?php echo sanitize_email(oviedo_set($options, 'email')); ?></strong></li>
                                    <li><?php echo wp_kses_post(oviedo_set($options, 'opening_hours')); ?></li>
                                </ul>
                            </div>
                        </div>
                        <?php endif; ?>
                        
                        <?php if(oviedo_set($options, 'button')): ?>
                        <!--Info Box-->
                        <div class="info-box">
                        	<!--Quote Btn-->
                        	<?php
if(function_exists('icl_get_languages')):
    $languages = icl_get_languages('skip_missing=1&orderby=custom');
     
    if(count($languages) >= 1):
         
        foreach((array)$languages as $language):            
            ?>
            <span class="icl-<?php echo $language['language_code']; ?><?php echo $language['active'] == 1 ? ' icl-current' : ''; ?>">          
                <a rel="alternate" hreflang="<?php echo $language['language_code']; ?>" href="<?php echo $language['url']; ?>"><img src="<?php echo $language['country_flag_url']; ?>" alt="<?php echo $language['native_name']; ?>" title="<?php echo $language['native_name']; ?>" /> <?php echo $language['native_name']; ?></a> 
            </span>
            <?php
        endforeach;
    endif;
endif;
?>
                        	<!--<div class="quote-btn"><a href="<?php echo esc_url(oviedo_set($options, 'btn_link')); ?>" class="btn-style-one theme-btn"><?php echo wp_kses_post(oviedo_set($options, 'btn_title')); ?></a></div>-->
                        </div>
                        <?php endif; ?>
                    </div>
                    
                </div>
            </div>
        </div>
        <!--End Header Upper-->
        
        <!--Header Lower-->
        <div class="header-lower">
            <div class="auto-container">
                <div class="outer-box clearfix">
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header clearfix">
                                <!-- Toggle Button -->    	
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_id' => 'navbar-collapse-1',
										'container_class'=>'navbar-collapse collapse navbar-right',
										'menu_class'=>'nav navbar-nav',
										'fallback_cb'=>false, 
										'items_wrap' => '%3$s', 
										'container'=>false,
										'walker'=> new Bunch_Bootstrap_walker()  
									) ); ?>
                                 </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->
                        
                        <!--Search form-->
                        <div class="search-form">
                            <?php get_template_part('searchform2')?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!--End Header Lower-->
        
        <!--Sticky Header-->
        <div class="sticky-header">
        	<div class="auto-container clearfix">
            	<!--Logo-->
            	<div class="logo pull-left">
                	<?php if(oviedo_set($options, 'responsive_logo')):?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="img-responsive"><img src="<?php echo esc_url(oviedo_set($options, 'responsive_logo'));?>" alt="<?php esc_html_e('Oviedo', 'oviedo');?>" title="<?php esc_html_e('Oviedo', 'oviedo');?>"></a>
                    <?php else:?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="img-responsive"><img src="<?php echo esc_url(get_template_directory_uri().'/images/logo-small.png');?>" alt="<?php esc_html_e('Oviedo', 'oviedo');?>"></a>
                    <?php endif;?>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                	<!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_id' => 'navbar-collapse-1',
									'container_class'=>'navbar-collapse collapse navbar-right',
									'menu_class'=>'nav navbar-nav',
									'fallback_cb'=>false, 
									'items_wrap' => '%3$s', 
									'container'=>false,
									'walker'=> new Bunch_Bootstrap_walker()  
								) ); ?>
                             </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
                
            </div>
        </div>
        <!--End Sticky Header-->
    
    </header>
    <!--End Main Header -->