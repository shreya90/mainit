<!--Price Section-->
<section class="price-page-section style-two">
    <div class="auto-container">
        
        <!--Pricing Outer Blocks-->
        <div class="pricing-outer-blocks style-two">
            <div class="row clearfix">
                <?php foreach( $atts['table'] as $key => $item ): ?>
                <!--Price Block Two-->
                <div class="price-block-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="plan-title"><?php echo wp_kses_post($item->title1); ?></div>
                        <div class="duration"><?php echo wp_kses_post($item->duration); ?></div>
                        <div class="price"><?php echo wp_kses_post($item->price); ?></div>
                        <ul class="specs-list">
                            <?php $fearures = explode("\n", ($item->feature_str));?>
							<?php foreach($fearures as $feature):?>
                                <li><?php echo wp_kses_post($feature ); ?></li>
                            <?php endforeach;?>
                        </ul>
                        <div class="link-box"><a href="<?php echo esc_url($item->btn_link); ?>" class="theme-btn btn-style-one"><?php echo wp_kses_post($item->btn_title); ?></a></div>
                    </div>
                </div>
                <?php endforeach;?> 
            </div>
        </div>
        <!--End Pricing Outer Blocks-->
        
    </div>    
    
</section>
<!--End Price Section-->