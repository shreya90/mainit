<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['testimonials_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ?>
<?php if($query->have_posts()):  ?>   

<!--Testimonial Section Four-->
<section class="testimonial-section-four">
    <div class="auto-container">
        <!--Sec Title Three-->
        <div class="sec-title-three">
            <div class="title"><?php echo wp_kses_post($title); ?></div>
            <h2><?php echo wp_kses_post($text); ?></h2>
        </div>
        
        <div class="testimonial-carousel owl-carousel owl-theme">
        	<?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$testimonial_meta = _WSH()->get_meta();
			?>
            <!--Testimonial Block Three-->
            <div class="testimonial-block-three">
                <div class="inner-box">
                    <div class="logo">
                        <img src="<?php echo esc_url(oviedo_set($testimonial_meta, 'test_logo_img')); ?>" alt="" />
                    </div>
                    <div class="text"><?php echo wp_kses_post(oviedo_trim(get_the_content(), $text_limit)); ?></div>
                    <div class="author-info">
                        <div class="author-inner">
                            <div class="image">
                                <?php the_post_thumbnail('oviedo_50x50'); ?>
                            </div>
                            <h3><?php the_title(); ?></h3>
                            <div class="designation"><?php echo wp_kses_post(oviedo_set($testimonial_meta, 'designation')); ?></div>
                        </div>
                        <div class="quote-icon"><span class="flaticon-right-quotation-mark"></span></div>
                    </div>
                </div>
            </div>
            <?php endwhile;?>
        </div>
        
    </div>
</section>
<!--End Testimonial Section Four-->

<?php endif;  wp_reset_postdata();  ?>