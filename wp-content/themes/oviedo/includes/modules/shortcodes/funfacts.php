<!--Counter Section-->
<section class="counter-section" style="background-image:url('<?php echo esc_url($bg_img); ?>');">
    <div class="auto-container">
        <!--Fact Counter-->
        <div class="fact-counter">
            <div class="row clearfix">
                <?php foreach( $atts['funfact'] as $key => $item ): ?>
                <!--Column-->
                <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                    <div class="inner">
                        <div class="count-outer count-box">
                            <span class="count-text" data-speed="2000" data-stop="<?php echo esc_attr($item->counter_stop); ?>"><?php echo wp_kses_post($item->counter_start); ?></span><span class="plus-icon"><?php echo wp_kses_post($item->plus_icon); ?></span>
                        </div>
                        <div class="counter-title"><?php echo wp_kses_post($item->title); ?></div>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>
        
    </div>
</section>
<!--End Counter Section-->