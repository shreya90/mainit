<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['testimonials_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ?>
<?php if($query->have_posts()):  ?>   

<!--Testimonial Section-->
<section class="testimonial-section-three">
    <div class="map-pattern"></div>
    <div class="auto-container">
        <!--Section Count-->
        <div class="section-count">
            <div class="count"><?php echo wp_kses_post($count_number); ?></div>
        </div>
        <div class="two-item-carousel owl-carousel owl-theme">
        	<?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$testimonial_meta = _WSH()->get_meta();
			?>
            <!--Testimonial Block Two-->
            <div class="testimonial-block-two">
                <div class="inner-box">
                    <div class="author">
                        <div class="image">
                            <?php the_post_thumbnail('oviedo_60x60');?>
                        </div>
                        <h3><?php the_title(); ?></h3>
                        <div class="designation"><?php echo wp_kses_post(oviedo_set($testimonial_meta, 'designation'));?></div>
                    </div>
                    <div class="text"><?php echo wp_kses_post(oviedo_trim(get_the_content(), $text_limit)); ?></div>
                </div>
            </div>
            <?php endwhile;?>
        </div>
    </div>
</section>
<!--End Testimonial Section-->

<?php endif;  wp_reset_postdata();  ?>