<!--Project Single Section-->
<section class="project-single">

    <div class="auto-container">
        <div class="inner-box">
            <!--Image-->
            <div class="image">
                <img src="<?php echo esc_url($image); ?>" alt="<?php esc_html_e('Image', 'oviedo'); ?>" />
            </div>
        
            <!--Project Title-->
            <div class="project-title">
                <div class="row clearfix">
                    <!--Column-->
                    <div class="column col-md-8 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                            <?php foreach( $atts['info'] as $key => $item ): ?>
                            <!--Info Column-->
                            <div class="info-column col-md-4 col-sm-4 col-xs-12">
                                <div class="inner">
                                    <h3><?php echo wp_kses_post($item->title); ?></h3>
                                    <div class="text"><?php echo wp_kses_post($item->text); ?></div>
                                </div>
                            </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                    <!--Btn Column-->
                    <div class="btn-column col-md-4 col-sm-12 col-xs-12">
                        <a href="<?php echo esc_url($btn_link); ?>" class="theme-btn btn-style-one"><?php echo wp_kses_post($btn_title); ?></a>
                    </div>
                </div>
            </div>
            
            <?php echo wp_kses_post($content_text); ?>
            
            <!--Project Title Box-->
            <div class="project-title-box">
                <div class="row clearfix">
                    <div class="column col-md-4 col-sm-12 col-xs-12">
                        <h2><?php echo wp_kses_post($sub_title); ?></h2>
                    </div>
                    <div class="column col-md-8 col-sm-12 col-xs-12">
                        <div class="text"><?php echo wp_kses_post($text1); ?></div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <!--Gallery Carousel Section-->
    <div class="gallery-carousel-section">
        <div class="project-single-carousel owl-carousel owl-theme">
            <?php foreach( $atts['projects'] as $key => $item ): ?>
            <!--Gallery Item-->
            <div class="gallery-item">
                <div class="inner-box">
                    <div class="image"><img src="<?php echo esc_url($item->proj_img); ?>" alt="">
                        <!--Overlay Box-->
                        <div class="overlay-box">
                            <div class="content">
                                <a href="<?php echo esc_url($ext_url); ?>"><span class="icon fa fa-link"></span></a>
                                <a class="lightbox-image" href="<?php echo esc_url($item->proj_img); ?>" title="<?php esc_html_e('Image Title Here', 'oviedo'); ?>" rel="gallery" data-fancybox-group="gallery"><span class="icon fa fa-search"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
    
    <!--Project Column Info-->
    <div class="project-column-info">
        <div class="auto-container">
            <div class="row clearfix">
                <?php foreach( $atts['overviews'] as $key => $item ): ?>
                <div class="column col-md-6 col-sm-6 col-xs-12">
                    <h2><?php echo wp_kses_post($item->title1); ?></h2>
                    <div class="text"><?php echo wp_kses_post($item->text2); ?></div>
                </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
    
</section>
<!--End End Project Single Section-->