<!--About Section-->
<section class="about-section">
    <div class="auto-container">
        <div class="row clearfix">
        
            <!--Count Column-->
            <div class="count-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <div class="content">
                        <div class="clearfix">
                            <!--Column-->
                            <div class="image-column pull-right col-md-6 col-sm-6 col-xs-12" style="background-image:url('<?php echo esc_url($image1); ?>');"></div>
                            <!--Column-->
                            <div class="column pull-left col-md-6 col-sm-6 col-xs-12">
                                <div class="column-inner">
                                    <div class="percent-text count-box"><span class="count-text" data-speed="2000" data-stop="<?php echo esc_attr($counter1); ?>"><?php esc_html_e('0', 'oviedo'); ?></span><?php esc_html_e('+', 'oviedo'); ?></div>
                                    <div class="text"><?php echo wp_kses_post($title1); ?></div>
                                </div>
                            </div>
                            <!--Column-->
                            <div class="image-column col-md-6 col-sm-6 col-xs-12" style="background-image:url('<?php echo esc_url($image2); ?>');"></div>
                            <!--Column-->
                            <div class="column col-md-6 col-sm-6 col-xs-12">
                                <div class="column-inner">
                                    <div class="percent-text count-box"><span class="count-text" data-speed="4000" data-stop="<?php echo esc_attr($counter2); ?>"><?php esc_html_e('0', 'oviedo'); ?></span><?php esc_html_e('+', 'oviedo'); ?></div>
                                    <div class="text"><?php echo wp_kses_post($title2); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Count Column-->
            
            <!--About Column-->
            <div class="about-column col-md-6 col-sm-12 col-xs-12">
                <div class="column-inner">
                    <h2><?php echo wp_kses_post($title); ?></h2>
                    <div class="text">
                        <?php echo wp_kses_post($text); ?>
                    </div>
                    <div class="clearfix">
                        <div class="author-info pull-left">
                            <div class="img-thumb">
                                <img src="<?php echo esc_url($author_img); ?>" alt="" />
                            </div>
                            <h4><?php echo wp_kses_post($author_title); ?></h4>
                            <div class="designation"><?php echo wp_kses_post($author_designation); ?></div>
                        </div>
                        <div class="pull-right signature">
                            <div class="signature-inner">
                                <img src="<?php echo esc_url($signature_img); ?>" alt="" />
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo esc_url($btn_link); ?>" class="theme-btn btn-style-four"><?php echo wp_kses_post($btn_title); ?></a>
                </div>
            </div>
            <!--About Column-->
            
        </div>
    </div>
</section>
<!--End About Section-->