<!--Sponsors Section-->
<section class="sponsors-section">
    <div class="auto-container">
        <div class="carousel-outer">
            <!--Sponsors Slider-->
            <ul class="sponsors-carousel owl-carousel owl-theme">
                <?php foreach( $atts['sponsors'] as $key => $item ): ?>
                <li><div class="image-box"><a href="<?php echo esc_url($item->ext_url); ?>"><img src="<?php echo esc_url($item->img); ?>" alt="<?php esc_html_e('Awesome Image', 'oviedo'); ?>"></a></div></li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</section>
<!--End Sponsors Section-->