<!--Performance Section-->
<section class="performance-section" style="background-image:url('<?php echo esc_url($bg_img); ?>');">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Progress Column-->
            <div class="progress-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner-box">
                    <div class="clearfix">
                        <div class="pull-left">
                            <div class="title"><?php echo wp_kses_post($year); ?></div>
                        </div>
                        <div class="pull-right">
                            <a href="<?php echo esc_url($btn_link1); ?>" class="detail"><?php echo wp_kses_post($btn_title1); ?></a>
                        </div>
                    </div>
                    <!--Two Column-->
                    <div class="two-column row clearfix">
                        <div class="column col-md-6 col-sm-6 col-xs-12">
                            <div class="image">
                                <img src="<?php echo esc_url($chart_img); ?>" alt="<?php esc_html_e('Awesome Image', 'oviedo'); ?>" />
                            </div>
                        </div>
                        <div class="column col-md-6 col-sm-6 col-xs-12">
                            <div class="percent-text count-box"><span class="count-text" data-speed="3000" data-stop="<?php echo esc_attr($counter_stop); ?>"><?php echo esc_attr($counter_start); ?></span><span class="percent"><?php esc_html_e('%', 'oviedo'); ?></span></div>
                            <div class="text"><?php echo wp_kses_post($sub_title); ?></div>
                            <ul class="progres-info">
                                <?php $fearures = explode("\n", ($feature_str));?>
								<?php foreach($fearures as $feature):?>
                                    <li><?php echo wp_kses_post($feature ); ?></li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--Content Column-->
            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner-box">
                    <h2><?php echo wp_kses_post($title); ?></h2>
                    <div class="text"><?php echo wp_kses_post($text); ?> </div>
                    <a href="<?php echo esc_url($btn_link); ?>" class="theme-btn btn-style-one"><?php echo wp_kses_post($btn_title); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Performance Section-->