<!--Services Section Five-->
<section class="services-section-five">
    <div class="auto-container">
        <h2><?php echo wp_kses_post($title); ?></h2>
        <div class="text"><?php echo wp_kses_post($text); ?></div>
        <div class="row clearfix">
            <?php foreach( $atts['service'] as $key => $item ): ?>
            <!--Services Block Seven-->
            <div class="services-block-seven col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon <?php echo esc_attr($item->icons); ?>"></span>
                    </div>
                    <h3><a href="<?php echo esc_url($item->ext_url); ?>"><?php echo wp_kses_post($item->ser_title); ?></a></h3>
                    <div class="inner-text"><?php echo wp_kses_post($item->ser_text); ?> </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</section>
<!--End Services Section Five-->