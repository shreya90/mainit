<!--Price Section-->
<section class="price-page-section">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title-four">
            <div class="row clearfix">
                <div class="column col-md-6 col-sm-6 col-xs-12">
                    <div class="big-letter"><?php echo wp_kses_post($big_alphabet); ?></div>
                    <h2><?php echo wp_kses_post($title); ?></h2>
                </div>
                <div class="column col-md-6 col-sm-6 col-xs-12">
                    <div class="text"><?php echo wp_kses_post($text); ?></div>
                </div>
            </div>
        </div>
        
        <!--Pricing Outer Blocks-->
        <div class="pricing-outer-blocks">
            <div class="row clearfix">
                <?php foreach( $atts['table'] as $key => $item ): ?>
                <!--Price Block-->
                <div class="price-block style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <h2><?php echo wp_kses_post($item->title1); ?></h2>
                        <span class="icon-box"><span class="icon flaticon-round"></span></span>
                        <div class="text"><?php echo wp_kses_post($item->text1); ?></div>
                        <div class="price"><?php echo wp_kses_post($item->price); ?></div>
                        <a href="<?php echo esc_url($item->btn_link); ?>" class="theme-btn btn-style-one"><?php echo wp_kses_post($item->btn_title); ?></a>
                    </div>
                </div>
                <?php endforeach;?> 
            </div>
        </div>
        <!--End Pricing Outer Blocks-->
        
    </div>    
    
</section>
<!--End Price Section-->