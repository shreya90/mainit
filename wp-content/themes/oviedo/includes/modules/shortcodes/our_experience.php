<!--Experiance Section-->
<section class="experiance-section <?php if($style_two == 'option_1') echo 'alternate'; ?>">
    <div class="auto-container">
        <!--Sec Title Three-->
        <div class="sec-title-three light centered">
            <div class="title"><?php echo wp_kses_post($title); ?></div>
            <h2><?php echo wp_kses_post($text); ?></h2>
        </div>
        <div class="row clearfix">
            <?php foreach( $atts['service'] as $key => $item ): ?>
            <!--Services Block-->
            <div class="services-block-six col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon <?php echo esc_attr($item->icons); ?>"></span>
                    </div>
                    <h3><a href="<?php echo esc_url($item->ext_url); ?>"><?php echo wp_kses_post($item->title1); ?></a></h3>
                    <div class="text"><?php echo wp_kses_post($item->text1); ?></div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</section>
<!--End Experiance Section-->