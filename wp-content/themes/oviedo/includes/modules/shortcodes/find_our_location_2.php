<!--Info section-->
<section class="info-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Column-->
            <div class="column col-md-8 col-sm-12 col-xs-12">
                <h2><?php echo wp_kses_post($title); ?></h2>
                <div class="map-box">
                    <div class="map-icon">
                        <span class="icon"><img src="<?php echo esc_url($icon_img); ?>" alt="<?php esc_html_e('Awesome', 'oviedo'); ?>" /></span>
                        <div class="map-detail-box">
                            <h3><?php echo wp_kses_post($mark_title); ?></h3>
                            <div class="text"><?php echo wp_kses_post($mark_text); ?></div>
                        </div>
                    </div>
                    <img src="<?php echo esc_url($map_img); ?>" alt="<?php esc_html_e('Awesome', 'oviedo'); ?>" />
                </div>
            </div>
            <!--Column-->
            <div class="column col-md-4 col-sm-12 col-xs-12">
                <h2><?php echo wp_kses_post($form_title); ?></h2>
                <div class="form-box">
                    <!-- Default Form -->
                    <div class="default-form">
                        <?php echo do_shortcode($contact_form); ?>    
                    </div>
                    <!--End Default Form -->
                </div>
            </div>
        </div>
        
        <div class="info-blocks">
            <div class="row clearfix">
                <?php foreach( $atts['location'] as $key => $item ): ?>
                <!--Map Info-->
                <div class="info-block col-md-4 col-sm-6 col-xs-12">
                    <div class="inner">
                        <div class="icon <?php echo esc_attr($item->icons); ?>"></div>
                        <h3><?php echo wp_kses_post($item->title1); ?></h3>
                        <div class="text"><?php echo wp_kses_post($item->text1); ?></div>
                    </div>
                </div>
                <?php endforeach;?> 
                
            </div>
        </div>
        
    </div>
</section>
<!--End Info section-->