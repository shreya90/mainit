<?php 
$paged = get_query_var('paged');
$args = array('post_type' => 'bunch_projects', 'showposts'=>$num, 'orderby'=>$sort, 'order'=>$order, 'paged'=>$paged);
$terms_array = explode(",",$exclude_cats);
if($exclude_cats) $args['tax_query'] = array(array('taxonomy' => 'projects_category','field' => 'id','terms' => $terms_array,'operator' => 'NOT IN',));
$query = new WP_Query($args);

$t = $GLOBALS['_bunch_base'];

$data_filtration = '';
$data_posts = '';
?>

<?php if( $query->have_posts() ):

ob_start();?>

	<?php $count = 0; 
	$fliteration = array();?>
	<?php while( $query->have_posts() ): $query->the_post();
		global  $post;
		$meta = get_post_meta( get_the_id(), '_bunch_projects_meta', true );//printr($meta);
		$meta1 = _WSH()->get_meta();
		$post_terms = get_the_terms( get_the_id(), 'projects_category');// printr($post_terms); exit();
		foreach( (array)$post_terms as $pos_term ) $fliteration[$pos_term->term_id] = $pos_term;
		$temp_category = get_the_term_list(get_the_id(), 'projects_category', '', ', ');
	?>
		<?php $post_terms = wp_get_post_terms( get_the_id(), 'projects_category'); 
		$term_slug = '';
		if( $post_terms ) foreach( $post_terms as $p_term ) $term_slug .= $p_term->slug.' ';?>		
           
            <?php 
				$post_thumbnail_id = get_post_thumbnail_id($post->ID);
				$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
		    ?> 
            
            <!--Gallery Item-->
            <div class="gallery-item masonry-item all col-lg-4 col-md-4 col-sm-6 col-xs-12 <?php echo esc_attr($term_slug); ?>">
                <div class="inner-box">
                    <div class="image">
                    	<?php if(oviedo_set($meta1, 'extra_height') == 'extra_height') 
							$image_size = 'oviedo_370x582'; 
						  elseif(oviedo_set($meta1, 'midle_height') == 'midle_height')
							$image_size = 'oviedo_370x460'; 
						  else
							$image_size = 'oviedo_370x350'; 
						  the_post_thumbnail($image_size);
						?>
                        <!--Overlay Box-->
                        <div class="overlay-box">
                            <div class="content">
                                <a href="<?php echo esc_url(oviedo_set($meta1, 'ext_url')); ?>"><span class="icon fa fa-link"></span></a>
                                <a class="lightbox-image" href="<?php echo esc_url($post_thumbnail_url);?>" title="<?php the_title_attribute(); ?>" rel="gallery" data-fancybox-group="gallery"><span class="icon fa fa-search"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
<?php endwhile;?>
  
<?php wp_reset_postdata();
$data_posts = ob_get_contents();
ob_end_clean();

endif ;
ob_start();?>	 

<?php $terms = get_terms(array('projects_category')); ?>

<!--Work Gallery Section-->
<section class="work-gallery-section masonry-section">
    <div class="auto-container">
        <div class="big-letter"><?php echo wp_kses_post($big_alphabet); ?></div>
        <!--Sec Title-->
        <div class="sec-title-four">
            <h2><?php echo wp_kses_post($title); ?></h2>
        </div>
        
        <!--Gallery-->
        <div class="sortable-masonry">
            <!--Filter-->
            <div class="filters clearfix">
                <ul class="filter-tabs filter-btns">
                    <li class="filter active" data-role="button" data-filter=".all"><?php esc_attr_e('All', 'oviedo');?></li>
                    <?php foreach( $fliteration as $t ): ?>
                    <li class="filter" data-role="button" data-filter=".<?php echo esc_attr(oviedo_set( $t, 'slug' )); ?>"><?php echo wp_kses_post(oviedo_set( $t, 'name')); ?></li>
                    <?php endforeach;?>
                </ul>
            </div>
            
            <div class="items-container row clearfix">
            	<?php echo wp_kses_post($data_posts); ?>
            </div>
            
        </div>
        
    </div>
    
</section>
<!--End End Gallery Section-->