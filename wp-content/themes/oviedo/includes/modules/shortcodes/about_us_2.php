<!--Fluid Section One-->
<section class="fluid-section-one style-two">
    <div class="outer-container clearfix">
        <!--Image Column-->
        <div class="image-column" style="background-image:url('<?php echo esc_url($about_img); ?>');">
            <div class="big-letter"><?php echo wp_kses_post($big_alphabet); ?></div>
            <figure class="image-box"><img src="<?php echo esc_url($about_img); ?>" alt=""></figure>
        </div>
        <!--Text Column-->
        <div class="text-column">
            <div class="inner">
                <!--Sec Title Five-->
                <div class="sec-title-five">
                    <div class="title"><?php echo wp_kses_post($sub_title); ?></div>
                    <h2><?php echo wp_kses_post($title); ?></h2>
                </div>
                <div class="text"><?php echo wp_kses_post($text); ?></div>
                <a class="learn-more" href="<?php echo esc_url($btn_link); ?>"><?php echo wp_kses_post($btn_title); ?> &nbsp;<span class="flaticon-arrow-pointing-to-right"></span></a>
            </div>
        </div>
    </div>
</section>
<!--End Fluid Section One-->