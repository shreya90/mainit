<!--Project Section-->
<section class="project-tab-section">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title-two">
            <div class="row clearfix">
                <div class="column col-md-5 col-sm-12 col-xs-12">
                    <div class="big-letter"><?php echo wp_kses_post($big_alphabet);?></div>
                </div>
                <div class="column col-md-7 col-sm-12 col-xs-12">
                    <div class="title"><?php echo wp_kses_post($title);?></div>
                    <h2><?php echo wp_kses_post($text);?></h2>
                </div>
            </div>
        </div>
        
        <!--Section Count-->
        <div class="section-count">
            <div class="count"><?php echo wp_kses_post($count_number);?></div>
        </div>
        
    </div>
    
    <!--Porfolio Tabs-->
    <div class="project-tab">
        <div class="auto-container">
        <!--Sec Title-->
            <div class="clearfix">
                
                <div class="tab-btns-box pull-left">
                    <!--Tabs Header-->
                    <div class="tabs-header">
                        <ul class="product-tab-btns clearfix">
                            <?php foreach( $atts['product_tabs'] as $key => $item ):?>
                            <li class="p-tab-btn <?php if($key == 1) echo 'active-btn'; ?>" data-tab="#p-tab-<?php echo esc_attr($key);?>"><?php echo wp_kses_post($item->tab_title);?></li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
                    
                <div class="pull-right">
                    <a href="<?php echo esc_url($btn_link);?>" class="see-all"><?php echo wp_kses_post($btn_title);?></a>
                </div>
                    
            </div>
        </div>
        <!--Tabs Content-->  
        <div class="p-tabs-content">
        	<?php foreach( $atts['product_tabs'] as $key => $item ):
				$num = $item->num;
				$cat = $item->cat;
				$sort = $item->sort;
				$order = $item->order;
			?>
            <!--Portfolio Tab / Active Tab-->
            <div class="p-tab <?php if($key == 1) echo 'active-tab'; ?>" id="p-tab-<?php echo esc_attr($key);?>">
                <div class="project-carousel owl-theme owl-carousel">
                    <?php  
					   $count = 1;
					   $query_args = array('post_type' => 'bunch_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
					   if( $cat ) $query_args['projects_category'] = $cat;
					   $query = new WP_Query($query_args); 
						
						if($query->have_posts()):
						 
						while($query->have_posts()): $query->the_post();
						global $post ;
						$project_meta = _WSH()->get_meta();
					?>
                    <?php 
						$post_thumbnail_id = get_post_thumbnail_id($post->ID);
						$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
					?>
                    <!--Gallery Item-->
                    <div class="gallery-item">
                        <div class="inner-box">
                            <div class="image"><?php the_post_thumbnail('oviedo_430x500');?>
                                <!--Overlay Box-->
                                <div class="overlay-box">
                                    <div class="content">
                                        <a href="<?php echo esc_url(oviedo_set($project_meta, 'ext_url')); ?>"><span class="icon fa fa-link"></span></a>
                                        <a class="lightbox-image" href="<?php echo esc_url($post_thumbnail_url);?>" title="<?php the_title_attribute(); ?>" data-fancybox-group="gallery"><span class="icon fa fa-search"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
					<?php endwhile; endif;?>
                </div>
            </div>
            
            <?php endforeach;?>
        </div>
        
    </div>
    
</section>
<!--End Project Section-->