<!--Case Section-->
<section class="case-section">
    <div class="auto-container">
        <!--Section Count-->
        <div class="section-count">
            <div class="count"><?php echo wp_kses_post($count_value); ?></div>
        </div>
        <div class="row clearfix">
            <!--Image Column-->
            <div class="image-column col-md-6 col-sm-12 col-xs-12">
                <div class="image">
                    <img src="<?php echo esc_url($image)?>" alt="<?php esc_html_e('Awesome Image', 'oviedo'); ?>" />
                    <div class="big-letter"><?php echo wp_kses_post($big_alphabit)?></div>
                </div>
            </div>
            <!--Content Column-->
            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                <!--Sec Title Two-->
                <div class="sec-title-two">
                    <div class="title"><?php echo wp_kses_post($sub_title); ?></div>
                    <h2><?php echo wp_kses_post($title); ?></h2>
                </div>
                <div class="text"><?php echo wp_kses_post($text); ?></div>
                
                <!--Percentage Bar-->
                <div class="percentage-bar">
                    <div class="row clearfix">
                        <?php foreach( $atts['case'] as $key => $item ): ?>
                        <div class="column col-md-6 col-sm-6 col-xs-12">
                            <div class="percentage">
                                <div class="inner">
                                    <div class="big-letter"><?php echo wp_kses_post($item->big_letter); ?></div>
                                    <div class="total"><?php echo wp_kses_post($item->value); ?> <span class="arrow <?php if($key == 2) echo 'down'; ?> <?php echo esc_attr($item->icons); ?>"></span></div>
                                    <div class="percent-text"><?php echo wp_kses_post($item->title1); ?></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>
<!--End Case Section-->
