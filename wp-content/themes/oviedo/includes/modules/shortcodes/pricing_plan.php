<!--Pricing Section-->
<section class="pricing-section">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title centered style-two">
            <h2><?php echo wp_kses_post($title); ?></h2>
            <div class="text"><?php echo wp_kses_post($text); ?></div>
        </div>
        <!--End Sec Title-->
        
        <div class="row clearfix">
            <?php foreach( $atts['table'] as $key => $item ): ?>
            <!--Pricing Block-->
            <div class="pricing-block col-md-6 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon <?php echo esc_attr($item->icons); ?>"></span>
                    </div>
                    <h3><?php echo wp_kses_post($item->title1); ?></h3>
                    <div class="price"><?php echo wp_kses_post($item->price); ?></div>
                    <div class="text"><?php echo wp_kses_post($item->text1); ?></div>
                    <div class="row clearfix">
                        <!--Column-->
                        
                        <div class="column col-md-6 col-sm-6 col-xs-12">
                            <ul class="list-style-one">
                                <?php $fearures_f = explode("\n", ($item->feature_str1));?>
      							<?php foreach($fearures_f as $feature_f):?>
                                    <?php echo wp_kses_post($feature_f ); ?>
                                <?php endforeach;?>
							</ul>
                        </div>
                        
                        <div class="column col-md-6 col-sm-6 col-xs-12">
                        	<ul class="list-style-one">
                            	<?php $fearures_s = explode("\n", ($item->feature_str2));?>
      							<?php foreach($fearures_s as $feature_s):?>
                                    <?php echo wp_kses_post($feature_s); ?>
                                <?php endforeach;?>
                            </ul>
                        </div>
                        
                    </div>
                    <a href="<?php echo esc_url($item->btn_link); ?>" class="theme-btn btn-style-one"><?php echo wp_kses_post($item->btn_title); ?></a>
                </div>
            </div>
            <?php endforeach;?>
        </div>
        
    </div>
</section>
<!--End Pricing Section-->