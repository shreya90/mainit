<!--Call To Action-->
<section class="call-to-action">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="column col-md-8 col-sm-12 col-xs-12">
                <div class="text"><?php echo wp_kses_post($text); ?></div>
            </div>
            <div class="column text-right col-md-4 col-sm-12 col-xs-12">
                <a href="<?php echo esc_url($btn_link); ?>" class="theme-btn btn-style-three"><?php echo wp_kses_post($btn_title); ?></a>
            </div>
        </div>
    </div>
</section>
<!--End Call To Action-->