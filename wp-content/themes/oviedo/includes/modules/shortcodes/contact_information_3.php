<section class="contact-section alternate" style="background-image:url(<?php echo esc_url($bg_img)?>);">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Form Column-->
            <div class="form-column col-md-6 col-sm-6 col-xs-12">
                <div class="column-inner">
                    
                    <!--Contact Form-->
                    <div class="contact-form">
                        <?php echo do_shortcode($contact_form); ?>
                    </div>
                    
                </div>
            </div>
            <!--Content Column-->
            <div class="content-column light col-md-6 col-sm-6 col-xs-12">
                <div class="inner">
                    <div class="sec-title-four light">
                        <div class="title"><?php echo wp_kses_post($sub_title); ?></div>
                        <h2><?php echo wp_kses_post($title); ?></h2>
                    </div>
                    <div class="text"><?php echo wp_kses_post($text); ?></div>
                    <h3><?php echo wp_kses_post($phone); ?></h3>
                </div>
            </div>
        </div>
    </div>
</section>