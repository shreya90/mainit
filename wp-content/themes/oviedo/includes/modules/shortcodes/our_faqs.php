<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_faqs' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['faqs_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   $left_arr = array();
   $right_arr = array();?>  
<?php if($query->have_posts()):  ?>   
 <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$faq_meta = _WSH()->get_meta();
					if($count > 2) $count = 1;
					$active_block = ( $query->current_post == 0 ) ? 'active-block' : '';
					$active = ( $query->current_post == 0 ) ? 'active' : '';
					$current = ( $query->current_post == 0 ) ? 'current' : '';
				?>
                <?php if( ($count == 1)):
					$left_arr[get_the_id()] = ' <li class="accordion block '.$active_block.'">
													<div class="acc-btn '.$active.'"><div class="icon-outer"><span class="icon icon-plus flaticon-plus"></span> <span class="icon icon-minus flaticon-minus"></span></div>'.get_the_title(get_the_id()).'</div>
													<div class="acc-content '.$current.'">
														<div class="content">
															<div class="text">
																<p>'.wp_kses_post(oviedo_trim(get_the_content(), $text_limit)).'</p>
															</div>
														</div>
													</div>
												</li>';
				?>
                <?php else:
					$right_arr[get_the_id()] = ' <li class="accordion block">
													<div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus flaticon-plus"></span> <span class="icon icon-minus flaticon-minus"></span></div>'.get_the_title(get_the_id()).'</div>
													<div class="acc-content">
														<div class="content">
															<div class="text">
																<p>'.wp_kses_post(oviedo_trim(get_the_content(), $text_limit)).'</p>
															</div>
														</div>
													</div>
												</li>';
				?>
                <?php endif; ?>
                <?php $count++; endwhile; ?>
<!--Services Style Two-->
<section class="faq-section">
    <div class="auto-container">
        <!--Faq Title-->
        <div class="faq-title">
            <h2><?php echo wp_kses_post($title); ?></h2>
            <div class="title-text"><?php echo wp_kses_post($text); ?></div>
            <!-- faq Form -->
            <div class="faq-search-box">
                <form method="get" action="<?php echo esc_url(home_url('/')); ?>">
                    <div class="form-group">
                        <input type="search" name="s" value="" placeholder="<?php esc_html_e('Search Your Answer', 'oviedo'); ?>" required>
                        <input type="hidden" name="post_type" value="bunch_faqs">
                        <button type="submit"><span class="icon fa fa-search"></span></button>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="row clearfix">
            
            <!--Faq Column-->
            <div class="faq-column col-md-6 col-sm-12 col-xs-12">
                <!--Accordian Block-->
                <div class="accordion-block">
                    <ul class="accordion-box style-two">
                        <?php foreach($left_arr as $key => $content):?>
                            <?php echo wp_kses_post($content);?>
                       <?php endforeach;?>
                    </ul>
                </div>
            </div>
            
            <!--Faq Column-->
            <div class="faq-column col-md-6 col-sm-12 col-xs-12">
                <!--Accordian Block-->
                <div class="accordion-block">
                    <ul class="accordion-box style-two">
                        <?php foreach($right_arr as $key => $right_content):?>
                            <?php echo wp_kses_post($right_content);?>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
    
    <!--Faq Form Section-->
    <section class="faq-form-section">
        <div class="auto-container">
        
            <!--Faq Form-->
            <div class="faq-form-box">
                <h2><?php echo wp_kses_post($form_title); ?></h2>
                <!--Contact Form-->
                <?php echo do_shortcode($contact_form); ?>
            </div>
            <!--End Faq Form-->
        </div>
    </section>
    
</section>

<?php  endif;  wp_reset_postdata(); ?>
