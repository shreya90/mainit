<!--Subscribe Section-->
<section class="subscribe-section">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="column col-md-7 col-sm-12 col-xs-12">
                <h2><?php echo wp_kses_post($title); ?></h2>
            </div>
            <div class="btn-column col-md-5 col-sm-12 col-xs-12">
                <a href="<?php echo esc_url($btn_link); ?>" class="theme-btn btn-style-three"><?php echo wp_kses_post($btn_title); ?></a>&ensp;&ensp;
                <a href="<?php echo esc_url($btn_link1); ?>" class="theme-btn btn-style-two"><?php echo wp_kses_post($btn_title1); ?></a>
            </div>
        </div>
    </div>
</section>
<!--End Subscribe Section-->