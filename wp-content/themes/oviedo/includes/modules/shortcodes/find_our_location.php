<!--Location Section-->
<section class="location-section">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title centered">
            <h2><?php echo wp_kses_post($title); ?></h2>
        </div>
        <div class="map-box">
            <div class="map-icon">
                <span class="icon"><img src="<?php echo esc_url($icon_img); ?>" alt="<?php esc_html_e('Awesome', 'oviedo'); ?>" /></span>
                <div class="map-detail-box">
                    <h3><?php echo wp_kses_post($mark_title); ?></h3>
                    <div class="text"><?php echo wp_kses_post($mark_text); ?></div>
                </div>
            </div>
            <img src="<?php echo esc_url($map_img); ?>" alt="<?php esc_html_e('Awesome', 'oviedo'); ?>" />
        </div>
        
        <!--Map Info Section-->
        <div class="map-info-section">
            <div class="row clearfix">
                <?php foreach( $atts['location'] as $key => $item ): ?> 
                <!--Column-->
                <div class="column col-md-4 col-sm-6 col-xs-12">
                    <div class="map-info">
                        <div class="inner">
                            <div class="icon <?php echo esc_attr($item->icons); ?>"></div>
                            <h3><?php echo wp_kses_post($item->title1); ?></h3>
                            <div class="text"><?php echo wp_kses_post($item->text1); ?></div>
                        </div>
                    </div>
                </div>
              	<?php endforeach;?>  
            </div>
        </div>
        <!--Map Info Section-->
        
    </div>
</section>
<!--End Location Section-->