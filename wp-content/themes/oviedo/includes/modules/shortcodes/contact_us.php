<!--Contact Info Section-->
<section class="contact-info-section">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title-four centered">
            <h2><?php echo wp_kses_post($title); ?></h2>
        </div>
        
        <div class="row clearfix">
            <?php foreach( $atts['information'] as $key => $item ): ?>
            <!--Contact Info Block-->
            <div class="contact-info-block col-md-4 col-sm-6 col-xs-12">
                <div class="inner">
                    <div class="icon-box">
                        <span class="icon <?php echo esc_attr($item->icons); ?>"></span>
                    </div>
                    <h3><?php echo wp_kses_post($item->title1); ?></h3>
                    <div class="text"><?php echo wp_kses_post($item->text); ?></div>
                    
					<?php if($item->style_two):?>
                    <!--Social Icon Two-->
                    <ul class="social-icon-two">
                        <?php echo wp_kses_post(oviedo_get_social_icons()); ?>
                    </ul>
                    <?php endif; ?>
                    
                </div>
            </div>
            <?php endforeach;?>
        </div>
        
    </div>    
    
</section>
<!--End Price Section-->