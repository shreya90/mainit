<!--Contact Fullwidth Section-->
<section class="contact-fullwidth-section">
    <div class="outer-container">
        <div class="clearfix">
            <!--Map Column-->
            <div class="map-column">
                <!--Map Outer-->
                <div class="map-outer">
                    <!--Map Canvas-->
                    <div class="map-canvas"
                        data-zoom="12"
                        data-lat="<?php echo esc_js($lat); ?>"
                        data-lng="<?php echo esc_js($long); ?>"
                        data-type="roadmap"
                        data-hue="#ffc400"
                        data-title="<?php echo esc_js($mark_title); ?>"
                        data-icon-path="<?php echo esc_url($img); ?>"
                        data-content="<?php echo esc_js($mark_address); ?><br><a href='mailto:<?php echo sanitize_email($email); ?>'><?php echo sanitize_email($email); ?></a>">
                    </div>
                    <!--Map Info-->
                </div>
            </div>
            <!--Form Column-->
            <div class="form-column">
                <div class="inner-column">
                    <h2><?php echo wp_kses_post($form_title); ?></h2>
                    <!--Contact Form-->
                    <div class="contact-form">
                        <?php echo do_shortcode($contact_form); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>