<!--Fluid Section One-->
<section class="fluid-section-one">
    <div class="outer-container clearfix">
        <!--Image Column-->
        <div class="image-column" style="background-image:url('<?php echo esc_url($bg_img); ?>');">
            <figure class="image-box"><img src="<?php echo esc_url($bg_img); ?>" alt="<?php esc_html_e('Image', 'oviedo'); ?>"></figure>
        </div>
        <!--Text Column-->
        <div class="text-column">
            <div class="inner">
                <!--Sec Title Three-->
                <div class="<?php if($title_style === 'two'): ?>sec-title-four<?php else:?>sec-title-three<?php endif; ?>">
                    <?php if($sub_title): ?>
                    	<div class="title"><?php echo wp_kses_post($sub_title); ?></div>
                    <?php endif; ?>
                    <h2><?php echo wp_kses_post($title); ?></h2>
                </div>
                <div class="text"><?php echo wp_kses_post($text); ?></div>
                <!--Percentage Bar-->
                <div class="percentage-bar">
                    <div class="row clearfix">
                        <?php foreach( $atts['case'] as $key => $item ): ?>
                        <div class="column col-md-6 col-sm-6 col-xs-12">
                            <div class="percentage">
                                <div class="percent-inner">
                                    <div class="total"><?php echo wp_kses_post($item->value); ?> <span class="arrow <?php if($key == 2) echo 'down'; ?> <?php echo esc_attr($item->icons); ?>"></span></div>
                                    <div class="percent-text"><?php echo wp_kses_post($item->title1); ?></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Fluid Section One-->