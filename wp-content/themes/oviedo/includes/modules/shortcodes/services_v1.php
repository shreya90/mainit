<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ?>
<?php if($query->have_posts()):  ?>    

<!--Services Section Eight-->
<section class="services-section-eight">
    <div class="auto-container">
        
        <div class="row clearfix">
            <!--Column-->
            <div class="column col-md-6 col-sm-6 col-xs-12">
                <div class="big-letter"><?php echo wp_kses_post($big_alphabet); ?></div>
                <!--Sec Title Four-->
                <div class="sec-title-four">
                    <h2><?php echo wp_kses_post($title); ?></h2>
                </div>
            </div>
            <!--Column-->
            <div class="column col-md-6 col-sm-6 col-xs-12">
                <div class="text"><?php echo wp_kses_post($text); ?> </div>
            </div>
        </div>
        
        <div class="outer-blocks">
            <div class="row clearfix">
                <?php while($query->have_posts()): $query->the_post();
					global $post ; 
					$services_meta = _WSH()->get_meta();
				?>
                <!--Services Block Eleven-->
                <div class="services-block-eleven col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="icon-box">
                            <span class="icon <?php echo str_replace("icon ", "", oviedo_set($services_meta, 'fontawesome'));?>"></span>
                        </div>
                        <h3><a href="<?php echo esc_url(oviedo_set($services_meta, 'ext_url')); ?>"><?php the_title(); ?></a></h3>
                        <div class="text"><?php echo wp_kses_post(oviedo_trim(get_the_content(), $text_limit)); ?></div>
                    </div>
                </div>
                <?php endwhile;?>
            </div>
        </div>
    </div>
    
</section>
<!--End Services Section Two-->

<?php endif;  wp_reset_postdata();  ?>