<!--Services Section-->
<section class="services-section-seven" style="background-image:url('<?php echo esc_url($bg_img); ?>')">
    <div class="auto-container">
        <!--Section Title-->
        <div class="title-box">
            <div class="big-letter"><?php echo wp_kses_post($big_alhpabit); ?></div>
            <div class="row clearfix">
                <!--Column-->
                <div class="title-column col-md-6 col-sm-12 col-xs-12">
                    <h2><?php echo wp_kses_post($title); ?></h2>
                </div>
                <!--Column-->
                <div class="title-column col-md-6 col-sm-12 col-xs-12">
                    <div class="text">
                        <?php echo wp_kses_post($text); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
        	<?php foreach( $atts['service'] as $key => $item ): ?>
            <!--Services Block Nine-->
            <div class="services-block-nine col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon <?php echo esc_attr($item->icons); ?>"></span>
                    </div>
                    <h3><a href="<?php echo esc_url($item->ext_url); ?>"><?php echo wp_kses_post($item->title1); ?></a></h3>
                    <div class="designation"><?php echo wp_kses_post($item->designation); ?></div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</section>
<!--End Services Section-->