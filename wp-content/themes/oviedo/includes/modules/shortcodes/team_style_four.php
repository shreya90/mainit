<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_team' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['team_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ?>
<?php if($query->have_posts()):  ?>   

<!--Team Section-->
<section class="team-section-three fullwidth-team">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title-four">
            <div class="row clearfix">
                <div class="column col-md-6 col-sm-6 col-xs-12">
                    <div class="big-letter"><?php echo wp_kses_post($big_alphabet); ?></div>
                    <h2><?php echo wp_kses_post($title); ?></h2>
                </div>
                <div class="column col-md-6 col-sm-6 col-xs-12">
                    <div class="text"><?php echo wp_kses_post($text); ?></div>
                </div>
            </div>
        </div>
    </div>    
    
    <!--Team Outer Blocks-->
    <div class="team-outer-blocks">
        <div class="clearfix">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$team_meta = _WSH()->get_meta();
			?>
            <!--Team Block-->
            <div class="team-block-four col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="image">
                        <a href="<?php echo esc_url(oviedo_set($team_meta, 'ext_url')); ?>"><?php the_post_thumbnail('oviedo_480x554'); ?></a>
                    </div>
                </div>
            </div>
            <?php endwhile;?>
        </div>
    </div>
        
</section>
<!--End Team Section-->

<?php endif; wp_reset_postdata(); ?>