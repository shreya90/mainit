<!--Contact Section-->
<section class="contact-section style-two padding-top">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Form Column-->
            <div class="form-column col-md-6 col-sm-6 col-xs-12">
                <div class="column-inner">
                    
                    <!--Contact Form-->
                    <div class="contact-form">
                        <?php echo do_shortcode($contact_form); ?>
                    </div>
                    
                </div>
            </div>
            <!--Info Column-->
            <div class="content-column col-md-6 col-sm-6 col-xs-12">
                <div class="inner">
                    <div class="sec-title-four">
                        <h2><?php echo wp_kses_post($title); ?></h2>
                    </div>
                    <div class="text"><?php echo wp_kses_post($text); ?></div>
                    <h3><?php echo wp_kses_post($phone); ?></h3>
                    <!--Social Icon Four-->
                    <?php if($style_two): ?>
                    <ul class="social-icon-four">
                        <?php echo wp_kses_post(oviedo_get_social_icons()); ?>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Contact Section-->