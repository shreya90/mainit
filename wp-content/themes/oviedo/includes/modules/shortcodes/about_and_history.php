<!--About Section Two-->
<section class="about-section-two style-two">
    <div class="auto-container">
        
        <div class="row clearfix">
            <!--Column-->
            <div class="column col-md-6 col-sm-12 col-xs-12">
                <!--Sec Title Two-->
                <div class="sec-title-two">
                    <div class="big-letter"><?php echo wp_kses_post($big_alphabit)?></div>
                    <div class="title"><?php echo wp_kses_post($title)?></div>
                    <h2><?php echo wp_kses_post($text)?></h2>
                </div>
            </div>
            <!--Column-->
            <div class="column col-md-6 col-sm-12 col-xs-12">
                <div class="text">
                    <?php echo wp_kses_post($description)?>
                </div>
            </div>
        </div>
    </div>
    
    <!--About Blocks Outer-->
    <div class="about-blocks-outer">
        <div class="auto-container">
            <?php foreach( $atts['service'] as $key => $item ): ?>
            <!--About Block-->
            <div class="about-block">
                <div class="inner-box">
                    <div class="row clearfix">
                        <!--Image Column-->
                        <div class="image-column col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-column">
                                <h2><?php echo wp_kses_post($item->sub_title); ?></h2>
                                <div class="image">
                                    <img src="<?php echo esc_url($item->img); ?>" alt="" />
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column col-md-6 col-sm-6 col-xs-12">
                            <div class="content-inner">
                                <!--Title-->
                                <div class="sec-title-four">
                                    <h2><?php echo wp_kses_post($item->title1); ?></h2>
                                </div>
                                <div class="text"><?php echo wp_kses_post($item->text1); ?> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
    <!--End About Blocks Outer-->
    
</section>
<!--End About Section Two-->