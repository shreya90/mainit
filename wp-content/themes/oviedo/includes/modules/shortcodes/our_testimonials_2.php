<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['testimonials_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ?>
<?php if($query->have_posts()):  ?>   

<!--Testimonial Section Two-->
<section class="testimonial-section-two">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title centered light style-two">
            <div class="title"><?php echo wp_kses_post($sub_title); ?></div>
            <h2><?php echo wp_kses_post($title); ?></h2>
        </div>
        
        <div class="two-item-carousel owl-carousel owl-theme">
        	<?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$testimonial_meta = _WSH()->get_meta();
			?>
            <!--Client Box-->
            <div class="client-box">
                <div class="inner-box">
                    <div class="content">
                        <div class="icon-box">
                            <?php the_post_thumbnail('oviedo_70x70'); ?>
                        </div>
                        <h3><?php the_title(); ?></h3>
                        <div class="designation"><?php echo wp_kses_post(oviedo_set($testimonial_meta, 'designation'));?></div>
                        <div class="text"><?php echo wp_kses_post(oviedo_trim(get_the_content(), $text_limit)); ?></div>
                    </div>
                </div>
            </div>
            <?php endwhile;?>
        </div>
        
        <div class="text-center">
            <a href="<?php echo esc_url($btn_link); ?>" class="theme-btn btn-style-two"><?php echo wp_kses_post($btn_title); ?></a>
        </div>
        
    </div>
</section>
<!--End Testimonial Section-->

<?php endif;  wp_reset_postdata();  ?>