<?php  
   global $post ;
   $count = 0;
   $paged = get_query_var('paged');
   $query_args = array('post_type' => 'post' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order, 'paged'=>$paged);
   if( $cat ) $query_args['category_name'] = $cat;
   $query = new WP_Query($query_args) ; 
   ?> 
<?php if($query->have_posts()):  ?>   

<!--Blog Section-->
<section class="blog-section blog-grid-fullwidth">
    <div class="outer-container">
        <div class="row clearfix">
        	<?php while($query->have_posts()): $query->the_post();
                global $post ; 
                $post_meta = _WSH()->get_meta();
            ?>
            <!--News Block-->
            <div class="news-block col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="image">
                        <a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>"><?php the_post_thumbnail('oviedo_580x360'); ?></a>
                    </div>
                    <div class="lower-box">
                        <div class="post-info"><?php the_author(); ?> /  <?php echo get_the_date('d F, Y'); ?> </div>
                        <h3><a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>"><?php the_title(); ?></a></h3>
                    </div>
                </div>
            </div>
            <?php endwhile;?>
        </div>
    </div> 
    <!-- Styled Pagination -->
    <div class="styled-pagination text-center">
        <?php oviedo_the_pagination(array('total'=>$query->max_num_pages, 'next_text' => '<div class="next"><i class="flaticon-arrow-pointing-to-right"></i></div>', 'prev_text' => '<div class="prev"><i class="flaticon-arrow-pointing-to-right"></i></div>')); ?>
    </div>
    
</section>
<!--End Blog Section-->

<?php endif; wp_reset_postdata();  ?>