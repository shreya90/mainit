<!--Price Section-->
<section class="price-page-section alternate">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title-four">
            <div class="row clearfix">
                <div class="column col-md-6 col-sm-6 col-xs-12">
                    <div class="big-letter"><?php echo wp_kses_post($big_alphabet); ?></div>
                    <h2><?php echo wp_kses_post($title); ?></h2>
                </div>
                <div class="column col-md-6 col-sm-6 col-xs-12">
                    <div class="text"><?php echo wp_kses_post($text); ?></div>
                </div>
            </div>
        </div>
        
        <!--Pricing Outer Blocks-->
        <div class="pricing-outer-blocks">
            <div class="row clearfix">
                <?php foreach( $atts['table'] as $key => $item ): ?>
                <!--Price Block Two-->
                <div class="price-block-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="plan-title"><?php echo wp_kses_post($item->title1); ?></div>
                        <div class="duration"><?php echo wp_kses_post($item->duration); ?></div>
                        <div class="price"><?php echo wp_kses_post($item->price); ?></div>
                        <ul class="specs-list">
                            <?php $fearures = explode("\n", ($item->feature_str));?>
							<?php foreach($fearures as $feature):?>
                                <li><?php echo wp_kses_post($feature ); ?></li>
                            <?php endforeach;?>
                        </ul>
                        <div class="link-box"><a href="<?php echo esc_url($item->btn_link); ?>" class="theme-btn btn-style-one"><?php echo wp_kses_post($item->btn_title); ?></a></div>
                    </div>
                </div>
                <?php endforeach;?> 
            </div>
        </div>
        <!--End Pricing Outer Blocks-->
        
    </div>    
    
</section>
<!--End Price Section-->