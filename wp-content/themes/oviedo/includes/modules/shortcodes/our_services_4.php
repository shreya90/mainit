<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ?>
<?php if($query->have_posts()):  ?>

<!--Gallery Section-->
<section class="gallery-section">
    <div class="auto-container">
        <!--Sec Title Three-->
        <div class="sec-title-three">
            <div class="clearfix">
                <div class="pull-left">
                    <div class="title"><?php echo wp_kses_post($title); ?></div>
                    <h2><?php echo wp_kses_post($text); ?></h2>
                </div>
                <div class="pull-right">
                    <a href="<?php echo esc_url($btn_link); ?>" class="theme-btn btn-style-one"><?php echo wp_kses_post($btn_title); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="five-item-carousel owl-carousel owl-theme">
    	<?php while($query->have_posts()): $query->the_post();
			global $post ; 
			$services_meta = _WSH()->get_meta();
		?>
        <!--Gallery Block-->
        <div class="gallery-block">
            <div class="inner-box">
                <div class="image">
                    <?php the_post_thumbnail('oviedo_384x480'); ?>
                    <div class="overlay-box">
                        <div class="content">
                            <h3><a href="<?php echo esc_url(oviedo_set($services_meta, 'ext_url')); ?>"><?php the_title(); ?></a></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile;?>  
    </div>
</section>
<!--End Gallery Section-->

<?php endif;  wp_reset_postdata();  ?>