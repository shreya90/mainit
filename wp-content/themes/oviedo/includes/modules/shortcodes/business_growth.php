<!--Growth Section-->
<section class="growth-section" style="background-image:url('<?php echo esc_url($bg_img); ?>');">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Content Column-->
            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                <div class="column-inner">
                    <h2><?php echo wp_kses_post($title); ?></h2>
                    <div class="text">
                        <?php echo wp_kses_post($text); ?>
                    </div>
                </div>
            </div>
            <!--Graph Column-->
            <div class="graph-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <div class="clearfix">
                        <div class="pull-left">
                            <h3><?php echo wp_kses_post($chart_title); ?></h3>
                        </div>
                        <div class="pull-right">
                            <a href="<?php echo esc_url($btn_link); ?>" class="detail-link"><?php echo wp_kses_post($btn_title); ?></a>
                        </div>
                    </div>
                    <div class="graph-img">
                        <img src="<?php echo esc_url($chart_img); ?>" alt="<?php esc_html_e('Awesome Image', 'oviedo'); ?>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Growth Section-->