<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['projects_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ?>
<?php if($query->have_posts()):  ?>

<!--Recent Work Section-->
<section class="recent-work-section">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title-five">
            <div class="row clearfix">
                <div class="column col-md-6 col-sm-12 col-xs-12">
                    <div class="big-letter"><?php echo wp_kses_post($big_alphabet); ?></div>
                    <div class="title"><?php echo wp_kses_post($title); ?></div>
                    <h2><?php echo wp_kses_post($text); ?></h2>
                </div>
                <div class="column col-md-6 col-sm-12 col-xs-12">
                    <a href="<?php echo esc_url($btn_link); ?>" class="theme-btn btn-style-one"><?php echo wp_kses_post($btn_title); ?></a>
                </div>
            </div>
        </div>
        
    </div>
    <br>
    <div class="outer-container">
        <div class="recent-work-carousel owl-theme owl-carousel">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$project_meta = _WSH()->get_meta();
			?> 
            <?php 
				$post_thumbnail_id = get_post_thumbnail_id($post->ID);
				$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
			?> 
            <!--Gallery Item-->
            <div class="gallery-item">
                <div class="inner-box">
                    <div class="image"><?php the_post_thumbnail('oviedo_430x500'); ?>
                        <!--Overlay Box-->
                        <div class="overlay-box">
                            <div class="content">
                                <a href="<?php echo esc_url(oviedo_set($project_meta, 'ext_url')); ?>"><span class="icon fa fa-link"></span></a>
                                <a class="lightbox-image" href="<?php echo esc_url($post_thumbnail_url);?>" title="<?php the_title_attribute(); ?>" data-fancybox-group="gallery"><span class="icon fa fa-search"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile;?>
        </div>
    </div>
</section>
<!--End Recent Work Section-->

<?php endif;  wp_reset_postdata();  ?>