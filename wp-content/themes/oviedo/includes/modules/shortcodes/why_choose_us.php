<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['testimonials_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ?>
<?php if($query->have_posts()):  ?>   

<!--fluid Section-->
<section class="fluid-section">
    <!--BG Image-->
    <div class="background-image" style="background-image:url('<?php echo esc_url($bg_img); ?>');"></div>
    
    <div class="auto-container">
        <div class="row clearfix">
            <div class="testimonial-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner">
                    <!--Testimonial Style-->
                    <div class="testimonial-inner">
                        <div class="quote-icon"><span class="icon flaticon-left-quote"></span></div>
                        <div class="single-item-carousel owl-carousel owl-theme">
                            <?php while($query->have_posts()): $query->the_post();
								global $post ; 
								$testimonial_meta = _WSH()->get_meta();
							?>
                            <div class="testimonial-box">
                                <div class="inner-box">
                                    <div class="text"><?php echo wp_kses_post(oviedo_trim(get_the_content(), $text_limit)); ?></div>
                                    <h4><?php the_title(); ?></h4>
                                </div>
                            </div>
                            <?php endwhile;?>
                        </div>
                    </div>
                </div>
            </div>
            <!--Content Column-->
            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner">
                    <h2><?php echo wp_kses_post($title); ?></h2>
                    
					<?php foreach( $atts['service'] as $key => $item ): ?>
                    <div class="featured-info-block">
                        <div class="inner-box">
                            <div class="icon-box">
                                <span class="icon <?php echo esc_attr($item->icons); ?>"></span>
                            </div>
                            <h3><a href="<?php echo esc_url($item->ext_url); ?>"><?php echo wp_kses_post($item->ser_title); ?></a></h3>
                            <div class="text"><?php echo wp_kses_post($item->ser_text); ?></div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End fluid Section-->

<?php endif;  wp_reset_postdata();  ?>