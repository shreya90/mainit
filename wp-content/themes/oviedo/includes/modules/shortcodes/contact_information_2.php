<!--Contact Section-->
<section class="contact-section style-two">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Form Column-->
            <div class="form-column col-md-6 col-sm-6 col-xs-12">
                <div class="column-inner">
                    
                    <!--Contact Form-->
                    <div class="contact-form">
                        <?php echo do_shortcode($contact_form); ?>
                    </div>
                    
                </div>
            </div>
            <!--Info Column-->
            <div class="content-column col-md-6 col-sm-6 col-xs-12">
                <div class="inner">
                    <div class="<?php if($title_style === 'two'): ?>sec-title-four<?php elseif($title_style === 'three'):?>sec-title-five<?php else:?>sec-title-three<?php endif; ?> ">
                        <div class="title"><?php echo wp_kses_post($upper_title); ?></div>
                        <h2><?php echo wp_kses_post($title); ?></h2>
                    </div>
                    <div class="text"><?php echo wp_kses_post($text); ?></div>
                    <h3><?php echo wp_kses_post($phone); ?></h3>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Contact Section-->