<!--About Section Two-->
<section class="about-section-two">
    <div class="auto-container">
        <!--Section Count-->
        <div class="section-count">
            <div class="count"><?php echo wp_kses_post($count_value); ?></div>
            <a href="<?php echo esc_url($link); ?>" class="video-box"><span class="icon flaticon-play-button"></span><?php echo wp_kses_post($vedio_title); ?></a>
        </div>
        <div class="row clearfix">
            <!--Column-->
            <div class="column col-md-6 col-sm-12 col-xs-12">
                <!--Sec Title Two-->
                <div class="sec-title-two">
                    <div class="big-letter"><?php echo wp_kses_post($big_alphabit)?></div>
                    <div class="title"><?php echo wp_kses_post($title)?></div>
                    <h2><?php echo wp_kses_post($text)?></h2>
                </div>
            </div>
            <!--Column-->
            <div class="column col-md-6 col-sm-12 col-xs-12">
                <div class="text">
                    <?php echo wp_kses_post($description)?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End About Section Two-->