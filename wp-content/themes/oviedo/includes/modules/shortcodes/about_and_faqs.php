<!--Deafult Section-->
<section class="default-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Column-->
            <div class="about-column column col-md-6 col-sm-12 col-xs-12">
                <div class="inner">
                    <div class="sec-title">
                        <h2><?php echo wp_kses_post($title); ?></h2>
                    </div>
                    <div class="text">
                        <?php echo wp_kses_post($text); ?>
                    </div>
                    <div class="clearfix">
                        <div class="author-info pull-left">
                            <div class="img-thumb">
                                <img src="<?php echo esc_url($author_img); ?>" alt="" />
                            </div>
                            <h4><?php echo wp_kses_post($author_title); ?></h4>
                            <div class="designation"><?php echo wp_kses_post($author_designation); ?></div>
                        </div>
                        <div class="pull-right signature">
                            <div class="signature-inner">
                                <img src="<?php echo esc_url($signature_img); ?>" alt="" />
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo esc_url($btn_link); ?>" class="theme-btn btn-style-two learn-more"><?php echo wp_kses_post($btn_title); ?></a>
                </div>
            </div>
            <!--Column-->
            <div class="column col-md-6 col-sm-12 col-xs-12">
                <div class="inner">
                    <div class="sec-title">
                        <h2><?php echo wp_kses_post($title1); ?></h2>
                        <div class="separater"></div>
                    </div>
                    
                    <!--Accordion Box-->
                    <ul class="accordion-box">
                    	<?php foreach( $atts['accordion'] as $key => $item ): ?>
                        <!--Block-->
                        <li class="accordion block">
                            <div class="acc-btn <?php if($key == 2) echo 'active'; ?>"><div class="icon-outer"><span class="icon icon-plus flaticon-plus-symbol"></span> <span class="icon icon-minus flaticon-minus-symbol"></span></div><?php echo wp_kses_post($item->acc_title); ?></div>
                            <div class="acc-content <?php if($key == 2) echo 'current'; ?>">
                                <div class="content">
                                    <p><?php echo wp_kses_post($item->acc_text); ?></p>
                                </div>
                            </div>
                        </li>
						<?php endforeach;?>
                    </ul>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Deafult Section-->