<!--Project Section Two-->
<section class="project-section-two">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Carousel Column-->
            <div class="carousel-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <!--Sec Title Three-->
                    <div class="sec-title-three">
                        <div class="title"><?php echo wp_kses_post($title);?></div>
                        <h2><?php echo wp_kses_post($text);?></h2>
                    </div>
                    <div class="single-item-carousel owl-carousel owl-theme">
                        <?php foreach( $atts['slider'] as $key => $item ):?>
                        <!--Project Block-->
                        <div class="project-block">
                            <div class="inner-box">
                                <h3><?php echo wp_kses_post($item->title1); ?></h3>
                                <div class="text"><?php echo wp_kses_post($item->text1); ?></div>
                                <a href="<?php echo esc_url($item->btn_link); ?>" class="theme-btn btn-style-one"><?php echo wp_kses_post($item->btn_title); ?></a>
                            </div>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
            <!--Tab Column-->
            <div class="tab-column col-md-6 col-sm-12 col-xs-12">
                <!--Porfolio Tabs-->
                <div class="project-tab style-two">
                    
                    <div class="tab-btns-box">
                        <!--Tabs Header-->
                        <div class="tabs-header">
                            <ul class="product-tab-btns clearfix">
                                <?php foreach( $atts['product_tabs'] as $key => $item ):?>
                                <li class="p-tab-btn <?php if($key == 1) echo 'active-btn'; ?>" data-tab="#p-tab-<?php echo esc_attr($key);?>"><?php echo wp_kses_post($item->tab_title);?></li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    </div>
                                
                    <!--Tabs Content-->  
                    <div class="p-tabs-content">
                    	<?php foreach( $atts['product_tabs'] as $key => $item ):
							$num = $item->num;
							$cat = $item->cat;
							$sort = $item->sort;
							$order = $item->order;
						?>
                        <!--Portfolio Tab / Active Tab-->
                        <div class="p-tab <?php if($key == 1) echo 'active-tab'; ?>" id="p-tab-<?php echo esc_attr($key);?>">
                            <div class="single-item-carousel owl-theme owl-carousel">
                                <?php  
								   $count = 1;
								   $query_args = array('post_type' => 'bunch_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
								   if( $cat ) $query_args['projects_category'] = $cat;
								   $query = new WP_Query($query_args); 
									
									if($query->have_posts()):
									 
									while($query->have_posts()): $query->the_post();
									global $post ;
									$project_meta = _WSH()->get_meta();
								?>
								<?php 
									$post_thumbnail_id = get_post_thumbnail_id($post->ID);
									$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
								?>
                                <!--Gallery Item-->
                                <div class="gallery-item">
                                    <div class="inner-box">
                                        <div class="image"><?php the_post_thumbnail('oviedo_574x440');?>
                                            <!--Overlay Box-->
                                            <div class="overlay-box">
                                                <div class="content">
                                                    <a href="<?php echo esc_url(oviedo_set($project_meta, 'ext_url')); ?>"><span class="icon fa fa-link"></span></a>
                                                    <a class="lightbox-image" href="<?php echo esc_url($post_thumbnail_url);?>" title="<?php the_title_attribute(); ?>" data-fancybox-group="gallery"><span class="icon fa fa-search"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endwhile; endif;?>
                            </div>
                        </div>
                        <?php endforeach;?>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Project Section Two-->