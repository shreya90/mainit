<!--Services Section Four-->
<section class="services-section-four">
    <div class="auto-container">
        <!--Services Title Section-->
        <div class="services-title-section">
            <div class="row clearfix">
                <div class="column col-md-6 col-sm-12 col-xs-12">
                    <h2><?php echo wp_kses_post($title); ?></h2>
                </div>
                <div class="column col-md-6 col-sm-12 col-xs-12">
                    <div class="text"><?php echo wp_kses_post($text); ?></div>
                    <a href="<?php echo esc_url($btn_link); ?>" class="learn-more"><?php echo wp_kses_post($btn_title); ?> &nbsp;<span class="arrow flaticon-right-arrow-2"></span></a>
                </div>
            </div>
        </div>
        
        <div class="row clearfix">
            <?php foreach( $atts['service'] as $key => $item ): ?>
            <!--Services Block Five-->
            <div class="services-block-five col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon <?php echo esc_attr($item->icons); ?>"></span>
                    </div>
                    <h3><a href="<?php echo esc_url($item->ext_link); ?>"><?php echo wp_kses_post($item->ser_title); ?></a></h3>
                    <div class="text"><?php echo wp_kses_post($item->ser_text); ?> </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
        
    </div>
</section>
<!--End Services Section Four-->