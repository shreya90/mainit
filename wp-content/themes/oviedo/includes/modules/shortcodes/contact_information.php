<!--Contact Section-->
<section class="contact-section">
    
    <div class="map-pattern" style="background-image:url('<?php echo esc_url($bg_img); ?>')"></div>
    
    <div class="auto-container">
        
        <!--Section Count-->
        <div class="section-count">
            <div class="count"><?php echo wp_kses_post($count_number); ?></div>
        </div>
        
        <div class="row clearfix">
            <!--Form Column-->
            <div class="form-column col-md-6 col-sm-6 col-xs-12">
                <div class="column-inner">
                    
                    <!--Contact Form-->
                    <div class="contact-form">
                        <?php echo do_shortcode($contact_form); ?>
                    </div>
                    
                </div>
            </div>
            <!--Info Column-->
            <div class="info-column col-md-6 col-sm-6 col-xs-12">
                <div class="inner">
                    <h2><?php echo wp_kses_post($title); ?></h2>
                    <div class="text"><?php echo wp_kses_post($text); ?></div>
                    <h3><?php echo wp_kses_post($phone); ?></h3>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Contact Section-->