<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ?>
<?php if($query->have_posts()):  ?>   

<!--Services Section Two-->
<section class="services-section-two">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title centered">
            <h2><?php echo wp_kses_post($title); ?></h2>
        </div>
        <div class="three-item-carousel owl-carousel owl-theme">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$services_meta = _WSH()->get_meta();
			?>
            <!--Services Block Two-->
            <div class="services-block-two">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon <?php echo str_replace("icon ", "", oviedo_set($services_meta, 'fontawesome'));?>"></span>
                    </div>
                    <h3><a href="<?php echo esc_url(oviedo_set($services_meta, 'ext_url')); ?>"><?php the_title(); ?></a></h3>
                    <div class="text"><?php echo wp_kses_post(oviedo_trim(get_the_content(), $text_limit));?></div>
                    <a href="<?php echo esc_url(oviedo_set($services_meta, 'ext_url')); ?>" class="theme-btn btn-style-one"><?php esc_html_e('details', 'oviedo'); ?></a>
                    <a href="<?php echo esc_url(oviedo_set($services_meta, 'ext_url')); ?>" class="arrow-box"><span class="arrow fa fa-angle-right"></span></a>
                </div>
            </div>
            <?php endwhile;?>
        </div>
    </div>
</section>
<!--End Services Section-->

<?php endif;  wp_reset_postdata();  ?>