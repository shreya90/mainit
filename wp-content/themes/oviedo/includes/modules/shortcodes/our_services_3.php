<!--Services Section-->
<section class="services-section-three">
    <div class="auto-container">
        
        <!--Section Count-->
        <div class="section-count">
            <div class="count"><?php echo wp_kses_post($count_value); ?></div>
        </div>
        
        <div class="row clearfix">
            <?php foreach( $atts['service'] as $key => $item ): ?>
            <!--Services Block-->
            <div class="services-block-four col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon <?php echo esc_attr($item->icons); ?>"></span>
                    </div>
                    <div class="post-title"><?php echo wp_kses_post($item->sub_title); ?></div>
                    <h3><a href="<?php echo esc_url($item->link); ?>"><?php echo wp_kses_post($item->ser_title); ?></a></h3>
                    <div class="text"><?php echo wp_kses_post($item->ser_text); ?> <a href="<?php echo esc_url($item->link); ?>" class="read-more"><?php echo wp_kses_post($item->btn_text); ?></a> </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
        
        <!--Services Text-->
        <div class="services-text">
            <div class="row clearfix">
                <div class="column col-md-8 col-sm-12 col-xs-12">
                    <h2><?php echo wp_kses_post($text); ?></h2>
                </div>
                <div class="btn-column col-md-4 col-sm-12 col-xs-12">
                    <a href="<?php echo esc_url($btn_link); ?>" class="theme-btn btn-style-six"><?php echo wp_kses_post($btn_title); ?></a>
                </div>
            </div>
        </div>
        
    </div>
</section>
<!--End Services Section-->