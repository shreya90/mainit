<!--Price Section-->
<section class="price-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Title Column-->
            <div class="title-column col-md-3 col-sm-12 col-xs-12">
                <div class="sec-title">
                    <h2><?php echo wp_kses_post($title); ?></h2>
                    <div class="text"><?php echo wp_kses_post($text); ?></div>
                </div>
            </div>
            <!--Price Column-->
            <div class="price-column col-md-9 col-sm-12 col-xs-12">
                <div class="row clearfix">
                    <?php foreach( $atts['table'] as $key => $item ): ?>
                    <!--Price Block-->
                    <div class="price-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <h2><?php echo wp_kses_post($item->title1); ?></h2>
                            <span class="icon-box"><span class="icon flaticon-round"></span></span>
                            <div class="text"><?php echo wp_kses_post($item->text1); ?></div>
                            <div class="price"><?php echo wp_kses_post($item->price); ?></div>
                            <a href="<?php echo esc_url($item->btn_link); ?>" class="theme-btn btn-style-one"><?php echo wp_kses_post($item->btn_title); ?></a>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Price Section-->