<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_team' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['team_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ?>
<?php if($query->have_posts()):  ?>   

<!--Team Section-->
<section class="team-section">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title centered style-two">
            <h2><?php echo wp_kses_post($title); ?></h2>
            <div class="text"><?php echo wp_kses_post($text); ?></div>
        </div>
        <!--End Sec Title-->
        <div class="row clearfix">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$team_meta = _WSH()->get_meta();
			?>
            <!--Team Block-->
            <div class="team-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="image">
                        <?php the_post_thumbnail('oviedo_160x161'); ?>
                    </div>
                    <h3><?php the_title(); ?></h3>
                    <div class="designation"><?php echo wp_kses_post(oviedo_set($team_meta, 'designation')); ?></div>
                    <div class="text"><?php echo wp_kses_post(oviedo_trim(get_the_content(), $text_limit)); ?></div>
                    <!--Social Icon Two-->
                    <?php if($socials = oviedo_set($team_meta, 'bunch_team_social')):?>
                    <ul class="social-icon-two">
                        <?php foreach($socials as $key => $value):?>
                        	<li><a href="<?php echo esc_url(oviedo_set($value, 'social_link'));?>"><span class="fa <?php echo esc_attr(oviedo_set($value, 'social_icon'));?>"></span></a></li>
                        <?php endforeach;?>
                    </ul>
                    <?php endif;?>
                </div>
            </div>
            <?php endwhile;?>
        </div>
    </div>
</section>
<!--End Team Section-->

<?php endif; wp_reset_postdata(); ?>