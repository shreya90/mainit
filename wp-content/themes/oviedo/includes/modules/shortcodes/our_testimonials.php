<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['testimonials_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ?>
<?php if($query->have_posts()):  ?>   

<!--Testimonial Section-->
<section class="testimonial-section" style="background-image:url('<?php echo esc_url($bg_img); ?>');">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Column-->
            <div class="column col-lg-6 col-md-4 col-sm-12 col-xs-12">
                
            </div>
            <!--Column-->
            <div class="testimonial-column col-lg-6 col-md-8 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <div class="quote-icon"><span class="icon fa fa-quote-left"></span></div>
                    <div class="single-item-carousel owl-carousel owl-theme">
						<?php while($query->have_posts()): $query->the_post();
                            global $post ; 
                            $testimonial_meta = _WSH()->get_meta();
                        ?>
                        <!--Testimonial Block-->
                        <div class="testimonial-block">
                            <div class="inner-box">
                                <?php the_content(); ?>
                                <div class="author-box">
                                    <div class="author-inner">
                                        <div class="image">
                                            <?php the_post_thumbnail('oviedo_60x55'); ?>
                                        </div>
                                        <h3><?php the_title(); ?></h3>
                                        <div class="designation"><?php echo wp_kses_post(oviedo_set($testimonial_meta, 'designation'));?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endwhile;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Testimonial Section-->

<?php endif;  wp_reset_postdata();  ?>