<!--Sponsors Section Three-->
<section class="sponsors-section-three">
    <div class="auto-container">
        <div class="carousel-outer">
            <!--Sponsors Slider-->
            <ul class="sponsors-carousel-two owl-carousel owl-theme">
                <?php foreach( $atts['sponsors'] as $key => $item ): ?>
                <li><div class="image-box"><a href="<?php echo esc_url($item->ext_url); ?>"><img src="<?php echo esc_url($item->img); ?>" alt="<?php esc_html_e('Awesome Image', 'oviedo'); ?>"></a></div></li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</section>
<!--End Sponsors Three Section-->