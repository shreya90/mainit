<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_team' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['team_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ?>
<?php if($query->have_posts()):  ?>   

<!--Team Section Two-->
<section class="team-section-two">
    <div class="auto-container">
        
        <!--Section Count-->
        <div class="section-count">
            <div class="count"><?php echo wp_kses_post($count_number); ?></div>
        </div>
        <!--Sec Title Two-->
        <div class="sec-title-two">
            <div class="title"><?php echo wp_kses_post($title); ?></div>
            <h2><?php echo wp_kses_post($text); ?></h2>
            <div class="big-letter"><?php echo wp_kses_post($big_alphabit); ?></div>
        </div>
        <div class="row clearfix">
            <?php while($query->have_posts()): $query->the_post();
				global $post ; 
				$team_meta = _WSH()->get_meta();
			?>
            <!--Team Block Two-->
            <div class="team-block-two col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="image">
                        <a href="<?php echo esc_url(oviedo_set($team_meta, 'ext_url')); ?>"><?php the_post_thumbnail('oviedo_370x450'); ?></a>
                    </div>
                    <div class="lower-content">
                        <h3><a href="<?php echo esc_url(oviedo_set($team_meta, 'ext_url')); ?>"><?php the_title(); ?></a></h3>
                        <div class="designation"><?php echo wp_kses_post(oviedo_set($team_meta, 'designation')); ?></div>
                    </div>
                </div>
            </div>
            <!--End Team Block Two-->
            <?php endwhile;?>
        </div>
    </div>
</section>
<!--End Team Section Two-->

<?php endif; wp_reset_postdata(); ?>