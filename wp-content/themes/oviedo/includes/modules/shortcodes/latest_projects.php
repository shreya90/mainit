<?php  
   $count = 1;
   $query_args = array('post_type' => 'bunch_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['projects_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   ?>
<?php if($query->have_posts()):  ?>

<!--Project Section-->
<section class="project-section grey-bg">
    <div class="auto-container">
        <!--Sec Title-->
        <div class="sec-title centered">
            <h2><?php echo wp_kses_post($title); ?></h2>
            <div class="text"><?php echo wp_kses_post($text); ?></div>
        </div>
    </div>
    
    <div class="four-item-carousel owl-carousel owl-theme">
    	<?php while($query->have_posts()): $query->the_post();
			global $post ; 
			$project_meta = _WSH()->get_meta();
		?>
        <!--Default Gallery Item-->
        <div class="default-gallery-item">
            <div class="inner-box">
                <figure class="image-box"><?php the_post_thumbnail('oviedo_480x400'); ?></figure>
                <!--Overlay Box-->
                <div class="overlay-box">
                    <div class="overlay-inner">
                        <div class="content">
                            <?php $term_list = wp_get_post_terms(get_the_id(), 'projects_category', array("fields" => "names")); ?>
 							<div class="category"><?php echo implode( ', ', (array)$term_list );?></div>
                            <h4><a href="<?php echo esc_url(oviedo_set($project_meta, 'ext_url')); ?>"><?php the_title(); ?></a></h4>
                            <a href="<?php echo esc_url(oviedo_set($project_meta, 'ext_url')); ?>" class="option-btn"><span class="flaticon-plus"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile;?>
    </div>
    
</section>
<!--End Project Section-->

<?php endif;  wp_reset_postdata();  ?>