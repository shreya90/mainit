<!--Location Section-->
<section class="location-section">
    <div class="auto-container">
        
        <div class="map-box">
            <div class="map-icon">
                <span class="icon"><img src="<?php echo esc_url($icon_img); ?>" alt="" /></span>
                <div class="map-detail-box">
                    <h3><?php echo wp_kses_post($mark_title); ?></h3>
                    <div class="text"><?php echo wp_kses_post($mark_text); ?></div>
                </div>
            </div>
            <img src="<?php echo esc_url($map_img); ?>" alt="<?php esc_html_e('Awesome', 'oviedo'); ?>" />
        </div>
        
    </div>
</section>
<!--End Location Section-->