<!--Pricing Section-->
<section class="pricing-section-two">
    <div class="auto-container">
        <div class="pricing-tabs">
            <div class="row clearfix">
                
                <!--Content Column-->
                <div class="content-column pull-right col-md-6 col-sm-12 col-xs-12">
                    <div class="inner">
                        <!--Title-->
                        <div class="sec-title-five">
                            <div class="title"><?php echo wp_kses_post($sub_title); ?></div>
                            <h2><?php echo wp_kses_post($title); ?></h2>
                        </div>
                        
                        <div class="text"><?php echo wp_kses_post($text); ?></div>
                        
                        <ul class="tab-btns clearfix">
                         	<?php foreach( $atts['table'] as $key => $item ): ?>
                         	<li class="tab-btn <?php if($key == 2) echo 'active-btn'; ?>" data-tab="#yearly-tab<?php echo esc_attr($key); ?>"><?php echo wp_kses_post($item->button_des); ?></li>
                        	<?php endforeach;?>
                        </ul>
                        
                    </div>
                </div>
                
                <!--Image Column-->
                <div class="image-column pull-left col-md-6 col-sm-12 col-xs-12">
                    <div class="inner clearfix">
                        <div class="big-letter"><?php echo wp_kses_post($big_alphabet); ?></div>
                        <div class="pr-content">
                            <?php foreach( $atts['table'] as $key => $item ): ?>
                            <!--Price Column-->
                            <div class="price-column pr-tab <?php if($key == 2) echo 'active-tab'; ?>" id="yearly-tab<?php echo esc_attr($key); ?>">
                                <div class="col-inner">
                                    <div class="plan-title"><?php echo wp_kses_post($item->title1); ?></div>
                                    <div class="duration"><?php echo wp_kses_post($item->duration); ?></div>
                                    <div class="price"><?php echo wp_kses_post($item->price); ?></div>
                                    <ul class="specs-list">
                                        <?php $fearures = explode("\n", ($item->feature_str));?>
										<?php foreach($fearures as $feature):?>
                                            <li><?php echo wp_kses_post($feature ); ?></li>
                                        <?php endforeach;?>
                                    </ul>
                                    <div class="link-box"><a href="<?php echo esc_url($item->btn_link); ?>" class="theme-btn btn-style-one"><?php echo wp_kses_post($item->btn_title); ?></a></div>
                                </div>
                            </div>
                            <?php endforeach;?>
                            
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>