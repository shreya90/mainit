<?php $options = _WSH()->option();
	oviedo_bunch_global_variable();
?>
    <!-- Main Header / Header Style One-->
    <header class="main-header header-style-one">
    
    	<!-- Main Box -->
    	<div class="main-box">
        	<div class="auto-container">
            	<div class="outer-container clearfix">
                    <!--Logo Box-->
                    <div class="logo-box">
                        <?php if(oviedo_set($options, 'logo_image')):?>
                            <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(oviedo_set($options, 'logo_image'));?>" alt="<?php esc_html_e('Oviedo', 'oviedo');?>" title="<?php esc_html_e('Oviedo', 'oviedo');?>"></a></div>
                        <?php else:?>
                            <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_template_directory_uri().'/images/logo.png');?>" alt="<?php esc_html_e('Oviedo', 'oviedo');?>"></a></div>
                        <?php endif;?>
                    </div>
                    
                    <?php if(oviedo_set($options, 'button')): ?>
                    <!--Btn Outer-->
                    <div class="btn-outer">
                        <a href="<?php echo esc_url(oviedo_set($options, 'btn_link')); ?>" class="theme-btn donate-btn btn-style-one"><?php echo wp_kses_post(oviedo_set($options, 'btn_title')); ?></a>
                    </div>
                    <?php endif; ?>
                    <!--Nav Outer-->
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->    	
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_id' => 'navbar-collapse-1',
										'container_class'=>'navbar-collapse collapse navbar-right',
										'menu_class'=>'nav navbar-nav',
										'fallback_cb'=>false, 
										'items_wrap' => '%3$s', 
										'container'=>false,
										'walker'=> new Bunch_Bootstrap_walker()  
									) ); ?>
                                </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->
                        
                    </div>
                    <!--Nav Outer End-->
                    
            	</div>    
            </div>
        </div>
    
    </header>
    <!--End Main Header -->