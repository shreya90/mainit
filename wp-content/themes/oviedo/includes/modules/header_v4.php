<?php $options = _WSH()->option();
	oviedo_bunch_global_variable();
?>
 	
    <!-- Main Header / Header Style Four-->
    <header class="main-header header-style-four">
    
    	<!--Header Top-->
    	<div class="header-top">
        	<div class="auto-container">
            	<div class="clearfix">
                	<?php if(oviedo_set($options, 'need_help')):?>
                    <div class="pull-left">
                    	<a href="<?php echo esc_url(oviedo_set($options, 'need_link')); ?>" class="help"><?php echo wp_kses_post(oviedo_set($options, 'need_help')); ?></a>
                    </div>
                    <?php endif; ?>
                    
                    <div class="pull-right">
                    	<?php if(oviedo_set($options, 'phone')):?>
                        <div class="contact"><?php echo sanitize_email(oviedo_set($options, 'email_address')); ?> <span><?php echo wp_kses_post(oviedo_set($options, 'phone')); ?></span></div>
                        <!--Social Icon One-->
                        <?php endif; ?>
                        
                        <?php if(oviedo_set($options, 'head_social')): ?>
							<?php if(oviedo_set( $options, 'social_media' ) && is_array( oviedo_set( $options, 'social_media' ) )): ?>
                                <ul class="social-icon-two">
                                    <?php $social_icons = oviedo_set( $options, 'social_media' );
										foreach( oviedo_set( $social_icons, 'social_media' ) as $social_icon ):
										if( isset( $social_icon['tocopy' ] ) ) continue; ?>
										<li><a href="<?php echo esc_url(oviedo_set( $social_icon, 'social_link')); ?>"><span class="fa <?php echo esc_attr(oviedo_set( $social_icon, 'social_icon')); ?>"></span></a></li>
									<?php endforeach; ?>
                                </ul>
                    		<?php endif;?>
						<?php endif;?>
                    </div>
                    
                </div>
            </div>
        </div>
    
    	<!-- Main Box -->
    	<div class="main-box">
        	<div class="auto-container">
            	<div class="outer-container clearfix">
                    <!--Logo Box-->
                    <div class="logo-box">
                        <?php if(oviedo_set($options, 'logo_image_4')):?>
                            <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(oviedo_set($options, 'logo_image_4'));?>" alt="<?php esc_html_e('Oviedo', 'oviedo');?>" title="<?php esc_html_e('Oviedo', 'oviedo');?>"></a></div>
                        <?php else:?>
                            <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(get_template_directory_uri().'/images/logo-4.png');?>" alt="<?php esc_html_e('Oviedo', 'oviedo');?>"></a></div>
                        <?php endif;?>
                    </div>
                    
                    <?php if(oviedo_set($options, 'button')): ?>
                    <!--Btn Outer-->
                    <div class="btn-outer">
                        <a href="<?php echo esc_url(oviedo_set($options, 'btn_link')); ?>" class="theme-btn donate-btn btn-style-eight"><?php echo wp_kses_post(oviedo_set($options, 'btn_title')); ?></a>
                    </div>
                    <?php endif; ?>
                    
                    <!--Nav Outer-->
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->    	
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_id' => 'navbar-collapse-1',
										'container_class'=>'navbar-collapse collapse navbar-right',
										'menu_class'=>'nav navbar-nav',
										'fallback_cb'=>false, 
										'items_wrap' => '%3$s', 
										'container'=>false,
										'walker'=> new Bunch_Bootstrap_walker()  
									) ); ?>
                                 </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->
                        
                    </div>
                    <!--Nav Outer End-->
                    
            	</div>    
            </div>
        </div>
        
        <!--Sticky Header-->
        <div class="sticky-header">
        	<div class="auto-container clearfix">
            	<!--Logo-->
            	<div class="logo pull-left">
                	<?php if(oviedo_set($options, 'responsive_logo_2')):?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="img-responsive"><img src="<?php echo esc_url(oviedo_set($options, 'responsive_logo_2'));?>" alt="<?php esc_html_e('Oviedo', 'oviedo');?>" title="<?php esc_html_e('Oviedo', 'oviedo');?>"></a>
                    <?php else:?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="img-responsive"><img src="<?php echo esc_url(get_template_directory_uri().'/images/logo-small-2.png');?>" alt="<?php esc_html_e('Oviedo', 'oviedo');?>"></a>
                    <?php endif;?>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                	<!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_id' => 'navbar-collapse-1',
									'container_class'=>'navbar-collapse collapse navbar-right',
									'menu_class'=>'nav navbar-nav',
									'fallback_cb'=>false, 
									'items_wrap' => '%3$s', 
									'container'=>false,
									'walker'=> new Bunch_Bootstrap_walker()  
								) ); ?>
                             </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
                
            </div>
        </div>
        <!--End Sticky Header-->
    
    </header>
    <!--End Main Header / Header Style Four-->