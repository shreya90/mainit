<?php oviedo_bunch_global_variable(); 
	$options = _WSH()->option();
	get_header(); 
	if( $wp_query->is_posts_page ) {
		$meta = _WSH()->get_meta('_bunch_layout_settings', get_queried_object()->ID);
		$meta1 = _WSH()->get_meta('_bunch_header_settings', get_queried_object()->ID);
		if(oviedo_set($_GET, 'layout_style')) $layout = oviedo_set($_GET, 'layout_style'); else
		$layout = oviedo_set( $meta, 'layout', 'right' );
		$sidebar = oviedo_set( $meta, 'sidebar', 'default-sidebar' );
		$bg = oviedo_set($meta1, 'header_img');
		$title = oviedo_set($meta1, 'header_title');
	} else {
		$settings  = _WSH()->option(); 
		if(oviedo_set($_GET, 'layout_style')) $layout = oviedo_set($_GET, 'layout_style'); else
		$layout = oviedo_set( $settings, 'archive_page_layout', 'right' );
		$sidebar = oviedo_set( $settings, 'archive_page_sidebar', 'default-sidebar' );
		$bg = oviedo_set($settings, 'archive_page_header_img');
		$title = oviedo_set($settings, 'archive_page_header_title');
	}
	$layout = oviedo_set( $_GET, 'layout' ) ? oviedo_set( $_GET, 'layout' ) : $layout;
	$sidebar = ( $sidebar ) ? $sidebar : 'default-sidebar';
	$layout = ( $layout ) ? $layout : 'right';
	_WSH()->page_settings = array('layout'=>'right', 'sidebar'=>$sidebar);
	$classes = ( !$layout || $layout == 'full' || oviedo_set($_GET, 'layout_style')=='full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12 ' : ' col-lg-9 col-md-8 col-sm-12 col-xs-12 ' ;
	?>
	
    <!--Page Title-->
    <section class="page-title" <?php if($bg):?>style="background-image:url(<?php echo esc_url($bg)?>);"<?php endif;?>>
        <div class="auto-container">
            <h1><?php if($title) echo wp_kses_post($title); else esc_html_e('Blog', 'oviedo');?></h1>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Blog Section-->
    <section class="blog-section">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!-- sidebar area -->
                <?php if( $layout == 'left' ): ?>
                	<?php if ( is_active_sidebar( $sidebar ) ) { ?>
                        <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">        
                            <aside class="sidebar">
                                <?php dynamic_sidebar( $sidebar ); ?>
                            </aside>
                        </div>
                	<?php } ?>
                <?php endif; ?>
                
                <!--Content Side-->	
                <div class="<?php echo esc_attr($classes);?>">
                    
                    <!--Blog Post-->
                    <div class="thm-unit-test">
					<?php while( have_posts() ): the_post();?>
                        <!-- blog post item -->
                        <!-- Post -->
                        <div id="post-<?php the_ID(); ?>" <?php post_class();?>>
                            <?php get_template_part( 'blog' ); ?>
                        <!-- blog post item -->
                        </div><!-- End Post -->
                    <?php endwhile;?>
                    </div>
                    <!--Pagination-->
                    <div class="styled-pagination">
                        <?php oviedo_the_pagination(); ?>
                    </div>
                    
                </div>
                <!--Content Side-->
                
                <!-- sidebar area -->
                <?php if( $layout == 'right' ): ?>
                	<?php if ( is_active_sidebar( $sidebar ) ) { ?>
                        <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">        
                            <aside class="sidebar">
                                <?php dynamic_sidebar( $sidebar ); ?>
                            </aside>
                        </div>
                	<?php } ?>
                <?php endif; ?>
                <!--Sidebar-->
            </div>
        </div>
	</section>
<?php get_footer(); ?>