<form method="get" action="<?php echo esc_url(home_url('/')); ?>">
	<div class="form-group">
		<input type="search" name="s" value="" placeholder="<?php esc_html_e('Search...', 'oviedo'); ?>" required>
		<button type="submit"><span class="icon fa fa-search"></span></button>
	</div>
</form>