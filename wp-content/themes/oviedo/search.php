<?php oviedo_bunch_global_variable();
	$options = _WSH()->option();
	get_header(); 
	$settings  = _WSH()->option(); 
	if(oviedo_set($_GET, 'layout_style')) $layout = oviedo_set($_GET, 'layout_style'); else
	$layout = oviedo_set( $settings, 'search_page_layout', 'right' );
	$sidebar = oviedo_set( $settings, 'search_page_sidebar', 'default-sidebar' );
	_WSH()->page_settings = array('layout'=>$layout, 'sidebar'=>$sidebar);
	
	$sidebar = ( $sidebar ) ? $sidebar : 'default-sidebar';
	$layout = ( $layout ) ? $layout : 'right';
	
	$classes = ( !$layout || $layout == 'full' || oviedo_set($_GET, 'layout_style')=='full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12 ' : ' col-lg-9 col-md-8 col-sm-12 col-xs-12 ' ;
	$bg = oviedo_set($settings, 'search_page_header_img');
	$title = oviedo_set($settings, 'search_page_header_title');
?>

<!--Page Title-->
<section class="page-title" <?php if($bg):?>style="background-image:url(<?php echo esc_url($bg)?>);"<?php endif;?>>
    <div class="auto-container">
        <h1><?php if($title) echo wp_kses_post($title); else wp_title('');?></h1>
        <?php echo wp_kses_post(oviedo_get_the_breadcrumb()); ?>
    </div>
</section>
<!--End Page Title-->

<!--Blog Section-->
<section class="blog-section">
    <div class="auto-container">
        <div class="row clearfix">
            
            <!-- sidebar area -->
			<?php if( $layout == 'left' ): ?>
				<?php if ( is_active_sidebar( $sidebar ) ) { ?>
                    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">        
                        <aside class="sidebar">
                            <?php dynamic_sidebar( $sidebar ); ?>
                        </aside>
                    </div>
				<?php } ?>
			<?php endif; ?>
            
            <?php if(have_posts()):?>
            <!--Content Side-->	
            <div class="<?php echo esc_attr($classes);?>">
                
                <!--Blog Post-->
                <div class="thm-unit-test">
				<?php while( have_posts() ): the_post();?>
                    <!-- blog post item -->
                    <!-- Post -->
                    <div id="post-<?php the_ID(); ?>" <?php post_class();?>>
                        <?php get_template_part( 'blog' ); ?>
                    <!-- blog post item -->
                    </div><!-- End Post -->
                <?php endwhile;?>
                </div>
                <!--Pagination-->
                <div class="styled-pagination">
                    <?php oviedo_the_pagination(); ?>
                </div>
            
            </div>
			<?php else : ?>
                <div class="<?php echo esc_attr($classes);?> blog_post_area eco-search">
                    <p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'oviedo' ); ?></p>
                    <aside class="">
                    <?php get_search_form(); ?>
                    </aside>
                </div>
			<?php endif; ?>
            <!--Content Side-->
            
            <!-- sidebar area -->
			<?php if( $layout == 'right' ): ?>
				<?php if ( is_active_sidebar( $sidebar ) ) { ?>
                    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">        
                        <aside class="sidebar">
                            <?php dynamic_sidebar( $sidebar ); ?>
                        </aside>
                    </div>
                <?php } ?>
			<?php endif; ?>
            <!--Sidebar-->
        </div>
    </div>
</section>

<?php get_footer(); ?>