<form method="get" action="<?php echo esc_url(home_url('/')); ?>">
    <div class="form-group">
        <input type="text" name="s" value="" placeholder="<?php esc_html_e('Search Here...', 'oviedo');?>" required>
        <button type="submit" class="search-btn"><span class="fa fa-search"></span></button>
    </div>
</form>