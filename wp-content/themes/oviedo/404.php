<?php
$options = _WSH()->option();
    get_header(); 
?>

<!--Error Section-->
<section class="error-section" style="background-image:url(<?php echo esc_url(get_template_directory_uri());?>/images/background/6.jpg);">
    <div class="auto-container">
        <div class="error-big-text"><?php esc_html_e('404', 'oviedo'); ?></div>
        <h2><?php esc_html_e('Oops!! Page Not Found', 'oviedo'); ?></h2>
        <div class="text"><?php esc_html_e('The page you are looking for was removed or might never existed.', 'oviedo'); ?></div>
        <div class="error-options">
            <a href="<?php echo esc_url(home_url('/')); ?>" class="theme-btn btn-style-six"><?php esc_html_e('Go Home', 'oviedo'); ?></a>
            <span class="or"><?php esc_html_e('Or', 'oviedo'); ?></span>
            <!-- Error Search Form -->
            <div class="error-search-box">
                <?php get_template_part('searchform4')?>
            </div>
        </div>
    </div>
</section>
<!--Error Section-->
  		
<?php get_footer(); ?>