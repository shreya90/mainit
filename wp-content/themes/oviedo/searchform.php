<div class="search-box error-search-box">
    <form action="<?php echo esc_url(home_url('/')); ?>" method="get">
        <div class="form-group">
            <input type="search" name="s" value="" placeholder="<?php esc_html_e('Search', 'oviedo');?>">
            <button type="submit"><span class="fa fa-search"></span></button>
        </div>
    </form>
</div>