<!--News Block-->
<div class="news-block">
    <div class="inner-box">
        <?php if(has_post_thumbnail()):?>
        <div class="image">
            <a href="<?php echo esc_url(get_the_permalink(get_the_id()));?>"><?php the_post_thumbnail('oviedo_1170x500'); ?></a>
        </div>
        <?php endif; ?>
        <div class="lower-box">
            <div class="post-info"><?php the_author(); ?> /  <?php echo get_the_date(); ?> </div>
            <h3><a href="<?php echo esc_url(get_the_permalink(get_the_id()));?>"><?php the_title(); ?></a></h3>
            <div class="text"><?php the_excerpt();?></div>
            <a href="<?php echo esc_url(get_the_permalink(get_the_id()));?>" class="more"><?php esc_html_e('Continue Reading', 'oviedo'); ?> <span class="arrow flaticon-right-arrow-2"></span></a>
        </div>
    </div>
</div>