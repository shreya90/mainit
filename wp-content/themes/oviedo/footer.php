<?php $options = get_option('oviedo'.'_theme_options');?>
	
    <div class="clearfix"></div>
	
	<!--Footer Style Two-->
    <footer class="footer-style-two alternate">
    	<div class="auto-container">
        	
            <?php if ( is_active_sidebar( 'footer-sidebar' ) ) { ?>
            	<?php if(!(oviedo_set($options, 'hide_upper_footer'))):?>
            
                    <!--Widgets Section-->
                    <div class="widgets-section">
                        <div class="row clearfix">
                            <?php dynamic_sidebar( 'footer-sidebar' ); ?>
                        </div>
                    </div>
            	<?php endif; ?> 
            <?php } ?>
            
			<?php if(!(oviedo_set($options, 'hide_bottom_footer'))):?>
            <!--Footer Bottom-->
            <?php if(oviedo_set($options, 'copyright')):?>
            <div class="footer-bottom">
            	<div class="row clearfix">
                	<div class="col-md-4 col-sm-12 col-xs-12">
                    	<div class="copyright"><?php echo wp_kses_post(oviedo_set($options, 'copyright'));?></div>
                    </div>
                    <!--Counter Column-->
                    
                    <div class="counter-column col-md-8 col-sm-12 col-xs-12">
                    	
                    </div>
                                        
                </div>
            </div>
            <?php endif;?>
            <?php endif;?>
        </div>
    </footer>
    <!--End Footer Style Two-->
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>

<?php wp_footer(); ?>
</body>
</html>