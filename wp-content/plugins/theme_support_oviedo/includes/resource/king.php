<?php
/**
 * Kingcomposer array
 *
 * @package Student WP
 * @author Shahbaz Ahmed <shahbazahmed9@hotmail.com>
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Restricted' );
}

$orderby = array(
				"type"			=>	"select",
				"label"			=>	esc_html__("Order By", BUNCH_NAME),
				"name"			=>	"sort",
				'options'		=>	array('date'=>esc_html__('Date', BUNCH_NAME),'title'=>esc_html__('Title', BUNCH_NAME) ,'name'=>esc_html__('Name', BUNCH_NAME) ,'author'=>esc_html__('Author', BUNCH_NAME),'comment_count' =>esc_html__('Comment Count', BUNCH_NAME),'random' =>esc_html__('Random', BUNCH_NAME) ),
				"description"	=>	esc_html__("Enter the sorting order.", BUNCH_NAME)
			);
$order = array(
				"type"			=>	"select",
				"label"			=>	esc_html__("Order", BUNCH_NAME),
				"name"			=>	"order",
				'options'		=>	(array('ASC'=>esc_html__('Ascending', BUNCH_NAME),'DESC'=>esc_html__('Descending', BUNCH_NAME) ) ),			
				"description"	=>	esc_html__("Enter the sorting order.", BUNCH_NAME)
			);
$number = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Number', BUNCH_NAME ),
				"name"			=>	"num",
				"description"	=>	esc_html__('Enter Number of posts to Show.', BUNCH_NAME )
			);
$text_limit = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Text Limit', BUNCH_NAME ),
				"name"			=>	"text_limit",
				"description"	=>	esc_html__('Enter text limit of posts to Show.', BUNCH_NAME )
			);
$title = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Title', BUNCH_NAME ),
				"name"			=>	"title",
				"description"	=>	esc_html__('Enter section title.', BUNCH_NAME )
			);
$sub_title = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Sub-Title', BUNCH_NAME ),
				"name"			=>	"sub_title",
				"description"	=>	esc_html__('Enter section subtitle.', BUNCH_NAME )
			);
$text  = array(
				"type"			=>	"textarea",
				"label"			=>	esc_html__('Text', BUNCH_NAME ),
				"name"			=>	"text",
				"description"	=>	esc_html__('Enter text to show.', BUNCH_NAME )
			);
$btn_title = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Button Title', BUNCH_NAME ),
				"name"			=>	"btn_title",
				"description"	=>	esc_html__('Enter section Button title.', BUNCH_NAME )
			);
$btn_link = array(
				"type"			=>	"text",
				"label"			=>	esc_html__('Button Link', BUNCH_NAME ),
				"name"			=>	"btn_link",
				"description"	=>	esc_html__('Enter section Button Link.', BUNCH_NAME )
			);
$bg_img = array(
				"type"			=>	"attach_image_url",
				"label"			=>	esc_html__('Background Image', BUNCH_NAME ),
				"name"			=>	"bg_img",
				'admin_label' 	=> 	false,
				"description"	=>	esc_html__('Choose Background image.', BUNCH_NAME )
			);

$options = array();


// Revslider Start.
$options['bunch_revslider']	=	array(
					'name' => esc_html__('Revslider', BUNCH_NAME),
					'base' => 'bunch_revslider',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show  Revolution slider.', BUNCH_NAME),
					'params' => array(
								array(
									'type' => 'dropdown',
									'label' => esc_html__('Choose Slider', BUNCH_NAME ),
									'name' => 'slider_slug',
									'options' => bunch_get_rev_slider( 0 ),
									'description' => esc_html__('Choose Slider', BUNCH_NAME )
								),
								array(
									"type"			=>	"checkbox",
									"label"			=>	esc_html__('Style Two', BUNCH_NAME ),
									"name"			=>	"style_two",
									'options' => array(
										'option_1' => 'Style Two',
									),
									"description"	=>	esc_html__('Choose whether you want to show The Background curve.', BUNCH_NAME  )
								),
							),
						);
//Business Solution
$options['bunch_business_solution']	=	array(
					'name' => esc_html__('Business Solution', BUNCH_NAME),
					'base' => 'bunch_business_solution',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Business Solution.', BUNCH_NAME),
					'params' => array(
								$title,
								$text,
								$number,
								$text_limit,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'services_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Company Achivment
$options['bunch_company_achivment']	=	array(
					'name' => esc_html__('Company Achivment', BUNCH_NAME),
					'base' => 'bunch_company_achivment',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Company Achivment.', BUNCH_NAME),
					'params' => array(
								esc_html__( 'Performance chart', BUNCH_NAME ) => array(
									$bg_img,
									array(
										"type"			=>	"editor",
										"label"			=>	esc_html__('Title And Year', BUNCH_NAME ),
										"name"			=>	"year",
										"description"	=>	esc_html__('Enter The Title And Year.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Button Title', BUNCH_NAME ),
										"name"			=>	"btn_title1",
										"description"	=>	esc_html__('Enter The Button Title.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('External Link', BUNCH_NAME ),
										"name"			=>	"btn_link1",
										"description"	=>	esc_html__('Enter The External Link.', BUNCH_NAME )
									),
									array(
										"type"			=>	"attach_image_url",
										"label"			=>	esc_html__('Chart Images', BUNCH_NAME ),
										"name"			=>	"chart_img",
										'admin_label' 	=> 	false,
										"description"	=>	esc_html__('Choose Chart images Url.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Counter Start', BUNCH_NAME ),
										"name"			=>	"counter_start",
										"description"	=>	esc_html__('Enter The Counter Start.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Counter Stop', BUNCH_NAME ),
										"name"			=>	"counter_stop",
										"description"	=>	esc_html__('Enter The Counter Stop.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Sub Title', BUNCH_NAME ),
										"name"			=>	"sub_title",
										"description"	=>	esc_html__('Enter The Sub Title.', BUNCH_NAME )
									),
									array(
										"type"			=>	"textarea",
										"label"			=>	esc_html__('Feature List', BUNCH_NAME ),
										"name"			=>	"feature_str",
										"description"	=>	esc_html__('Enter The Feature List.', BUNCH_NAME )
									),
								),
								esc_html__( 'Company Information', BUNCH_NAME ) => array(
									array(
										"type"			=>	"textarea",
										"label"			=>	esc_html__('Title', BUNCH_NAME ),
										"name"			=>	"title",
										"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
									),
									$text,
									$btn_title,
									$btn_link,
								),
							),
						);
//About and Faqs
$options['bunch_about_and_faqs']	=	array(
					'name' => esc_html__('About and Faqs', BUNCH_NAME),
					'base' => 'bunch_about_and_faqs',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The About and Faqs.', BUNCH_NAME),
					'params' => array(
								esc_html__( 'About Us', BUNCH_NAME ) => array(
									$title,
									$text,
									array(
										"type"			=>	"attach_image_url",
										"label"			=>	esc_html__('Author Images', BUNCH_NAME ),
										"name"			=>	"author_img",
										'admin_label' 	=> 	false,
										"description"	=>	esc_html__('Choose Author images Url.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Author Title', BUNCH_NAME ),
										"name"			=>	"author_title",
										"description"	=>	esc_html__('Enter The Author Title.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Author Designation', BUNCH_NAME ),
										"name"			=>	"author_designation",
										"description"	=>	esc_html__('Enter The Author Designation.', BUNCH_NAME )
									),
									array(
										"type"			=>	"attach_image_url",
										"label"			=>	esc_html__('Signature Images', BUNCH_NAME ),
										"name"			=>	"signature_img",
										'admin_label' 	=> 	false,
										"description"	=>	esc_html__('Choose Signature images Url.', BUNCH_NAME )
									),
									$btn_title,
									$btn_link,
								),
								esc_html__( 'Our Faqs', BUNCH_NAME ) => array(
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Title', BUNCH_NAME ),
										"name"			=>	"title1",
										"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
									),
									array(
										'type' => 'group',
										'label' => esc_html__( 'Our Accordion', BUNCH_NAME ),
										'name' => 'accordion',
										'description' => esc_html__( 'Enter the Our Accordion.', BUNCH_NAME ),
										'params' => array(
													array(
														"type"			=>	"text",
														"label"			=>	esc_html__('Title', BUNCH_NAME ),
														"name"			=>	"acc_title",
														"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
													),
													array(
														"type"			=>	"textarea",
														"label"			=>	esc_html__('Text', BUNCH_NAME ),
														"name"			=>	"acc_text",
														"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
													),
												),
											),
										),
									),
								);
//Funfacts
$options['bunch_funfacts']	=	array(
					'name' => esc_html__('Funfacts', BUNCH_NAME),
					'base' => 'bunch_funfacts',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Funfacts.', BUNCH_NAME),
					'params' => array(
								$bg_img,
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Funfact', BUNCH_NAME ),
									'name' => 'funfact',
									'description' => esc_html__( 'Enter the Our Funfact.', BUNCH_NAME ),
									'params' => array(
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Counter Start', BUNCH_NAME ),
													"name"			=>	"counter_start",
													"description"	=>	esc_html__('Enter The Counter Start.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Counter Stop', BUNCH_NAME ),
													"name"			=>	"counter_stop",
													"description"	=>	esc_html__('Enter The Counter Stop.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Plus Sign', BUNCH_NAME ),
													"name"			=>	"plus_icon",
													"description"	=>	esc_html__('Enter The Plus Sign.', BUNCH_NAME )
												),
											),
										),
									),
								);
//Our Services
$options['bunch_our_services']	=	array(
					'name' => esc_html__('Our Services', BUNCH_NAME),
					'base' => 'bunch_our_services',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Services.', BUNCH_NAME),
					'params' => array(
								$title,
								$number,
								$text_limit,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'services_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Latest Project
$options['bunch_latest_projects']	=	array(
					'name' => esc_html__('Latest Project', BUNCH_NAME),
					'base' => 'bunch_latest_projects',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Latest Project.', BUNCH_NAME),
					'params' => array(
								$title,
								$text,
								$number,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'projects_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Call To Action
$options['bunch_call_to_action']	=	array(
					'name' => esc_html__('Call To Action', BUNCH_NAME),
					'base' => 'bunch_call_to_action',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Call To Action.', BUNCH_NAME),
					'params' => array(
								$text,
								$btn_title,
								$btn_link,
							),
						);
//Membership
$options['bunch_membership']	=	array(
					'name' => esc_html__('Membership', BUNCH_NAME),
					'base' => 'bunch_membership',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Membership.', BUNCH_NAME),
					'params' => array(
								$title,
								$text,
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Table', BUNCH_NAME ),
									'name' => 'table',
									'description' => esc_html__( 'Enter the Our Table.', BUNCH_NAME ),
									'params' => array(
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Text', BUNCH_NAME ),
													"name"			=>	"text1",
													"description"	=>	esc_html__('Enter The Plus Sign.', BUNCH_NAME )
												),
												array(
													"type"			=>	"editor",
													"label"			=>	esc_html__('Price', BUNCH_NAME ),
													"name"			=>	"price",
													"description"	=>	esc_html__('Enter The Price.', BUNCH_NAME )
												),
												$btn_title,
												$btn_link,
											),
										),
									),
								);
//Our Testimonials
$options['bunch_our_testimonials']	=	array(
					'name' => esc_html__('Our Testimonials', BUNCH_NAME),
					'base' => 'bunch_our_testimonials',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Testimonials.', BUNCH_NAME),
					'params' => array(
								$bg_img,
								$number,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'testimonials_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Find Our Location
$options['bunch_find_our_location']	=	array(
					'name' => esc_html__('Find Our Location', BUNCH_NAME),
					'base' => 'bunch_find_our_location',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Find Our Location.', BUNCH_NAME),
					'params' => array(
								$title,
								array(
									"type"			=>	"attach_image_url",
									"label"			=>	esc_html__('Background Map Images', BUNCH_NAME ),
									"name"			=>	"map_img",
									'admin_label' 	=> 	false,
									"description"	=>	esc_html__('Choose Background Map images Url.', BUNCH_NAME )
								),
								array(
									"type"			=>	"attach_image_url",
									"label"			=>	esc_html__('Marker Images', BUNCH_NAME ),
									"name"			=>	"icon_img",
									'admin_label' 	=> 	false,
									"description"	=>	esc_html__('Choose Marker images Url.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Mark Title', BUNCH_NAME ),
									"name"			=>	"mark_title",
									"description"	=>	esc_html__('Enter The Mark Title.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Mark Text', BUNCH_NAME ),
									"name"			=>	"mark_text",
									"description"	=>	esc_html__('Enter The Mark Text.', BUNCH_NAME )
								),
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Information', BUNCH_NAME ),
									'name' => 'location',
									'description' => esc_html__( 'Enter the Our Information.', BUNCH_NAME ),
									'params' => array(
												array(
													'type' => 'icon_picker',
													'label' => esc_html__( 'Icon', BUNCH_NAME ),
													'name' => 'icons',
													'description' => esc_html__( 'Enter The Icon.', BUNCH_NAME ),
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Text', BUNCH_NAME ),
													"name"			=>	"text1",
													"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
												),
											),
										),
									),
								);
//Our Services 2
$options['bunch_our_services_2']	=	array(
					'name' => esc_html__('Our Services 2', BUNCH_NAME),
					'base' => 'bunch_our_services_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Services 2.', BUNCH_NAME),
					'params' => array(
								$title,
								$text,
								$number,
								$text_limit,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'services_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//About Oviedo
$options['bunch_about_oviedo']	=	array(
					'name' => esc_html__('About Oviedo', BUNCH_NAME),
					'base' => 'bunch_about_oviedo',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The About Oviedo.', BUNCH_NAME),
					'params' => array(
								esc_html__( 'General', BUNCH_NAME ) => array(
									array(
										"type"			=>	"attach_image_url",
										"label"			=>	esc_html__('Images First', BUNCH_NAME ),
										"name"			=>	"image1",
										'admin_label' 	=> 	false,
										"description"	=>	esc_html__('Choose images Url.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Counter Stop', BUNCH_NAME ),
										"name"			=>	"counter1",
										"description"	=>	esc_html__('Enter The Counter Stop.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Expert Title', BUNCH_NAME ),
										"name"			=>	"title1",
										"description"	=>	esc_html__('Enter The Expert Title.', BUNCH_NAME )
									),
									array(
										"type"			=>	"attach_image_url",
										"label"			=>	esc_html__('Images First', BUNCH_NAME ),
										"name"			=>	"image2",
										'admin_label' 	=> 	false,
										"description"	=>	esc_html__('Choose images Url.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Counter Stop', BUNCH_NAME ),
										"name"			=>	"counter2",
										"description"	=>	esc_html__('Enter The Counter Stop.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Project Title', BUNCH_NAME ),
										"name"			=>	"title2",
										"description"	=>	esc_html__('Enter The Project Title.', BUNCH_NAME )
									),
								),
								esc_html__( 'About Oviedo', BUNCH_NAME ) => array(
									$title,
									$text,
									array(
										"type"			=>	"attach_image_url",
										"label"			=>	esc_html__('Author Images', BUNCH_NAME ),
										"name"			=>	"author_img",
										'admin_label' 	=> 	false,
										"description"	=>	esc_html__('Choose Author images Url.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Author Title', BUNCH_NAME ),
										"name"			=>	"author_title",
										"description"	=>	esc_html__('Enter The Author Title.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Author Designation', BUNCH_NAME ),
										"name"			=>	"author_designation",
										"description"	=>	esc_html__('Enter The Author Designation.', BUNCH_NAME )
									),
									array(
										"type"			=>	"attach_image_url",
										"label"			=>	esc_html__('Signature Images', BUNCH_NAME ),
										"name"			=>	"signature_img",
										'admin_label' 	=> 	false,
										"description"	=>	esc_html__('Choose Signature images Url.', BUNCH_NAME )
									),
									$btn_title,
									$btn_link,
								),
							),
						);
//Business Growth
$options['bunch_business_growth']	=	array(
					'name' => esc_html__('Business Growth', BUNCH_NAME),
					'base' => 'bunch_business_growth',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Business Growth.', BUNCH_NAME),
					'params' => array(
								$bg_img,
								array(
									"type"			=>	"editor",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Chart Title', BUNCH_NAME ),
									"name"			=>	"chart_title",
									"description"	=>	esc_html__('Enter The Chart Title.', BUNCH_NAME )
								),
								$btn_title,
								$btn_link,
								array(
									"type"			=>	"attach_image_url",
									"label"			=>	esc_html__('Chart Images', BUNCH_NAME ),
									"name"			=>	"chart_img",
									'admin_label' 	=> 	false,
									"description"	=>	esc_html__('Choose Chart images Url.', BUNCH_NAME )
								),
							),
						);
//Pricing Plan
$options['bunch_pricing_plan']	=	array(
					'name' => esc_html__('Pricing Plan', BUNCH_NAME),
					'base' => 'bunch_pricing_plan',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Pricing Plan.', BUNCH_NAME),
					'params' => array(
								$title,
								$text,
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Table', BUNCH_NAME ),
									'name' => 'table',
									'description' => esc_html__( 'Enter the Our Table.', BUNCH_NAME ),
									'params' => array(
												array(
													'type' => 'icon_picker',
													'label' => esc_html__( 'Icon', BUNCH_NAME ),
													'name' => 'icons',
													'description' => esc_html__( 'Enter The Icon.', BUNCH_NAME ),
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"editor",
													"label"			=>	esc_html__('Price', BUNCH_NAME ),
													"name"			=>	"price",
													"description"	=>	esc_html__('Enter The Price.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Text', BUNCH_NAME ),
													"name"			=>	"text1",
													"description"	=>	esc_html__('Enter The Plus Sign.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Feature List Left', BUNCH_NAME ),
													"name"			=>	"feature_str1",
													"description"	=>	esc_html__('Enter The Feature List Left.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Feature List Second', BUNCH_NAME ),
													"name"			=>	"feature_str2",
													"description"	=>	esc_html__('Enter The Feature List Second.', BUNCH_NAME )
												),
												$btn_title,
												$btn_link,
											),
										),
									),
								);
//Why Choose Us
$options['bunch_why_choose_us']	=	array(
					'name' => esc_html__('Why Choose Us', BUNCH_NAME),
					'base' => 'bunch_why_choose_us',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Why Choose Us.', BUNCH_NAME),
					'params' => array(
								esc_html__( 'Our Testimonials', BUNCH_NAME ) => array(
									$bg_img,
									$number,
									$text_limit,
									array(
										"type" => "dropdown",
										"label" => __( 'Category', BUNCH_NAME),
										"name" => "cat",
										"options" =>  bunch_get_categories(array( 'taxonomy' => 'testimonials_category'), true),
										"description" => __( 'Choose Category.', BUNCH_NAME)
									),
									$order,
									$orderby,
								),
								esc_html__( 'Why Choose Us', BUNCH_NAME ) => array(
									$title,
									array(
										'type' => 'group',
										'label' => esc_html__( 'Our Service', BUNCH_NAME ),
										'name' => 'service',
										'description' => esc_html__( 'Enter the Our Service.', BUNCH_NAME ),
										'params' => array(
													array(
														'type' => 'icon_picker',
														'label' => esc_html__( 'Icon', BUNCH_NAME ),
														'name' => 'icons',
														'description' => esc_html__( 'Enter The Icon.', BUNCH_NAME ),
													),
													array(
														"type"			=>	"text",
														"label"			=>	esc_html__('Title', BUNCH_NAME ),
														"name"			=>	"ser_title",
														"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
													),
													array(
														"type"			=>	"text",
														"label"			=>	esc_html__('External Link', BUNCH_NAME ),
														"name"			=>	"ext_url",
														"description"	=>	esc_html__('Enter The External Link.', BUNCH_NAME )
													),
													array(
														"type"			=>	"textarea",
														"label"			=>	esc_html__('Text', BUNCH_NAME ),
														"name"			=>	"ser_text",
														"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
													),
												),
											),
										),
									),
								);
//Our Team
$options['bunch_our_team']	=	array(
					'name' => esc_html__('Our Team', BUNCH_NAME),
					'base' => 'bunch_our_team',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Team.', BUNCH_NAME),
					'params' => array(
								$title,
								$text,
								$number,
								$text_limit,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'team_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Our Testimonials 2
$options['bunch_our_testimonials_2']	=	array(
					'name' => esc_html__('Our Testimonials 2', BUNCH_NAME),
					'base' => 'bunch_our_testimonials_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Testimonials 2.', BUNCH_NAME),
					'params' => array(
								$sub_title,
								array(
									"type"			=>	"editor",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$number,
								$text_limit,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'testimonials_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
								$btn_title,
								$btn_link,
							),
						);
//Find Our Location 2
$options['bunch_find_our_location_2']	=	array(
					'name' => esc_html__('Find Our Location 2', BUNCH_NAME),
					'base' => 'bunch_find_our_location_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Find Our Location 2.', BUNCH_NAME),
					'params' => array(
								$title,
								array(
									"type"			=>	"attach_image_url",
									"label"			=>	esc_html__('Background Map Images', BUNCH_NAME ),
									"name"			=>	"map_img",
									'admin_label' 	=> 	false,
									"description"	=>	esc_html__('Choose Background Map images Url.', BUNCH_NAME )
								),
								array(
									"type"			=>	"attach_image_url",
									"label"			=>	esc_html__('Marker Images', BUNCH_NAME ),
									"name"			=>	"icon_img",
									'admin_label' 	=> 	false,
									"description"	=>	esc_html__('Choose Marker images Url.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Mark Title', BUNCH_NAME ),
									"name"			=>	"mark_title",
									"description"	=>	esc_html__('Enter The Mark Title.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Mark Text', BUNCH_NAME ),
									"name"			=>	"mark_text",
									"description"	=>	esc_html__('Enter The Mark Text.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Form Title', BUNCH_NAME ),
									"name"			=>	"form_title",
									"description"	=>	esc_html__('Enter The Form Title.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Contact Form', BUNCH_NAME ),
									"name"			=>	"contact_form",
									"description"	=>	esc_html__('Enter The Contact Form.', BUNCH_NAME )
								),
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Information', BUNCH_NAME ),
									'name' => 'location',
									'description' => esc_html__( 'Enter the Our Information.', BUNCH_NAME ),
									'params' => array(
												array(
													'type' => 'icon_picker',
													'label' => esc_html__( 'Icon', BUNCH_NAME ),
													'name' => 'icons',
													'description' => esc_html__( 'Enter The Icon.', BUNCH_NAME ),
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Text', BUNCH_NAME ),
													"name"			=>	"text1",
													"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
												),
											),
										),
									),
								);
//Our Partners
$options['bunch_our_partners']	=	array(
					'name' => esc_html__('Our Partners', BUNCH_NAME),
					'base' => 'bunch_our_partners',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Partners.', BUNCH_NAME),
					'params' => array(
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Sponsors', BUNCH_NAME ),
									'name' => 'sponsors',
									'description' => esc_html__( 'Enter the Our Sponsors.', BUNCH_NAME ),
									'params' => array(
												array(
													"type"			=>	"attach_image_url",
													"label"			=>	esc_html__('Sponsors Images', BUNCH_NAME ),
													"name"			=>	"img",
													'admin_label' 	=> 	false,
													"description"	=>	esc_html__('Choose Sponsors images Url.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('External Link', BUNCH_NAME ),
													"name"			=>	"ext_url",
													"description"	=>	esc_html__('Enter The External Link.', BUNCH_NAME )
												),
											),
										),
									),
								);
//Call To Action 2
$options['bunch_call_to_action_2']	=	array(
					'name' => esc_html__('Call To Action 2', BUNCH_NAME),
					'base' => 'bunch_call_to_action_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Call To Action 2.', BUNCH_NAME),
					'params' => array(
								$title,
								$btn_title,
								$btn_link,
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Next Button Title', BUNCH_NAME ),
									"name"			=>	"btn_title1",
									"description"	=>	esc_html__('Enter The Next Button Title.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Next Button Link', BUNCH_NAME ),
									"name"			=>	"btn_link1",
									"description"	=>	esc_html__('Enter The Next Button Link.', BUNCH_NAME )
								),
							),
						);
//About Us
$options['bunch_about_us']	=	array(
					'name' => esc_html__('About Us', BUNCH_NAME),
					'base' => 'bunch_about_us',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The About Us.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Counter Number', BUNCH_NAME ),
									"name"			=>	"count_value",
									"description"	=>	esc_html__('Enter The Counter Number.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Video Link', BUNCH_NAME ),
									"name"			=>	"link",
									"description"	=>	esc_html__('Enter The Video Link.', BUNCH_NAME )
								),
								array(
									"type"			=>	"editor",
									"label"			=>	esc_html__('Video Title', BUNCH_NAME ),
									"name"			=>	"vedio_title",
									"description"	=>	esc_html__('Enter The Video Title.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabit', BUNCH_NAME ),
									"name"			=>	"big_alphabit",
									"description"	=>	esc_html__('Enter The Big AlphaBit.', BUNCH_NAME )
								),
								$title,
								$text,
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Description', BUNCH_NAME ),
									"name"			=>	"description",
									"description"	=>	esc_html__('Enter The Description.', BUNCH_NAME )
								),
							),
						);
//Our Services 3
$options['bunch_our_services_3']	=	array(
					'name' => esc_html__('Our Services 3', BUNCH_NAME),
					'base' => 'bunch_our_services_3',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Services 3.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Counter Number', BUNCH_NAME ),
									"name"			=>	"count_value",
									"description"	=>	esc_html__('Enter The Counter Number.', BUNCH_NAME )
								),
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Service', BUNCH_NAME ),
									'name' => 'service',
									'description' => esc_html__( 'Enter the Our Service.', BUNCH_NAME ),
									'params' => array(
												array(
													'type' => 'icon_picker',
													'label' => esc_html__( 'Icon', BUNCH_NAME ),
													'name' => 'icons',
													'description' => esc_html__( 'Enter The Icon.', BUNCH_NAME ),
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Post Title', BUNCH_NAME ),
													"name"			=>	"sub_title",
													"description"	=>	esc_html__('Enter The Post Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"ser_title",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Text', BUNCH_NAME ),
													"name"			=>	"ser_text",
													"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Button Title', BUNCH_NAME ),
													"name"			=>	"btn_text",
													"description"	=>	esc_html__('Enter The Button Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('External Link', BUNCH_NAME ),
													"name"			=>	"link",
													"description"	=>	esc_html__('Enter The External Title.', BUNCH_NAME )
												),
											),
										),
										$text,
										$btn_title,
										$btn_link,
									),
								);
//Case Study
$options['bunch_case_study']	=	array(
					'name' => esc_html__('Case Study', BUNCH_NAME),
					'base' => 'bunch_case_study',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Case Study.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Counter Number', BUNCH_NAME ),
									"name"			=>	"count_value",
									"description"	=>	esc_html__('Enter The Counter Number.', BUNCH_NAME )
								),
								array(
									"type"			=>	"attach_image_url",
									"label"			=>	esc_html__('Images', BUNCH_NAME ),
									"name"			=>	"image",
									'admin_label' 	=> 	false,
									"description"	=>	esc_html__('Choose images Url.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabit', BUNCH_NAME ),
									"name"			=>	"big_alphabit",
									"description"	=>	esc_html__('Enter The Big Alphabit.', BUNCH_NAME )
								),
								$sub_title,
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Cases', BUNCH_NAME ),
									'name' => 'case',
									'description' => esc_html__( 'Enter the Our Cases.', BUNCH_NAME ),
									'params' => array(
												array(
													'type' => 'icon_picker',
													'label' => esc_html__( 'Icon', BUNCH_NAME ),
													'name' => 'icons',
													'description' => esc_html__( 'Enter The Icon.', BUNCH_NAME ),
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Big Alphabit', BUNCH_NAME ),
													"name"			=>	"big_letter",
													"description"	=>	esc_html__('Enter The Big Alphabit.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Value', BUNCH_NAME ),
													"name"			=>	"value",
													"description"	=>	esc_html__('Enter The Value.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
											),
										),
									),
								);
//Our Testimonials 3
$options['bunch_our_testimonials_3']	=	array(
					'name' => esc_html__('Our Testimonials 3', BUNCH_NAME),
					'base' => 'bunch_our_testimonials_3',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Testimonials 3.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Counter Number', BUNCH_NAME ),
									"name"			=>	"count_number",
									"description"	=>	esc_html__('Enter The Counter Number.', BUNCH_NAME )
								),
								$number,
								$text_limit,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'testimonials_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Our Partners 2
$options['bunch_our_partners_2']	=	array(
					'name' => esc_html__('Our Partners 2', BUNCH_NAME),
					'base' => 'bunch_our_partners_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Partners 2.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Sponsors', BUNCH_NAME ),
									'name' => 'sponsors',
									'description' => esc_html__( 'Enter the Our Sponsors.', BUNCH_NAME ),
									'params' => array(
												array(
													"type"			=>	"attach_image_url",
													"label"			=>	esc_html__('Sponsors Images', BUNCH_NAME ),
													"name"			=>	"img",
													'admin_label' 	=> 	false,
													"description"	=>	esc_html__('Choose Sponsors images Url.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('External Link', BUNCH_NAME ),
													"name"			=>	"ext_url",
													"description"	=>	esc_html__('Enter The External Link.', BUNCH_NAME )
												),
											),
										),
									),
								);
//Our Work
$options['bunch_our_work'] = array(
						'name' => esc_html__('Our Work', BUNCH_NAME),
						'base' => 'bunch_our_work',
						'class' => '',
						'category' => esc_html__('Oviedo', BUNCH_NAME),
						'icon' => 'fa-briefcase',
						'description' => esc_html__('Show Our Work.', BUNCH_NAME),
						'params' => array(
									array(
										'type' => 'text',
										'label' => esc_html__( 'Big Alphabet', BUNCH_NAME ),
										'name' => 'big_alphabet',
										'description' => esc_html__( 'Enter The Alphabet .', BUNCH_NAME ),
									),
									$title,
									$text,
									array(
										'type' => 'text',
										'label' => esc_html__( 'Count Number', BUNCH_NAME ),
										'name' => 'count_number',
										'description' => esc_html__( 'Enter The Number .', BUNCH_NAME ),
									),
									$btn_title,
									$btn_link,
									//Group Start
									array(
										 'type' => 'group',
										 'label' => esc_html__( 'Project Tab', BUNCH_NAME ),
										 'name' => 'product_tabs',
										 'description' => esc_html__( 'Enter Project Tab Info.', BUNCH_NAME ),
										 'params' => array(
												array(
													 'type' => 'text',
													 'label' => esc_html__( 'Title', BUNCH_NAME ),
													 'name' => 'tab_title',
													 'description' => esc_html__( 'Enter Title of Menu Type .', BUNCH_NAME ),
												),
												$number,
												array(
													"type" => "dropdown",
													"label" => __( 'Category', BUNCH_NAME),
													"name" => "cat",
													"options" =>  bunch_get_categories(array( 'taxonomy' => 'projects_category'), true),
													"description" => __( 'Choose Category.', BUNCH_NAME)
												),
												$order,
												$orderby,
												
										 ),
									),//Group End
								),
							);

//Our Team 2
$options['bunch_our_team_2']	=	array(
					'name' => esc_html__('Our Team 2', BUNCH_NAME),
					'base' => 'bunch_our_team_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Team 2.', BUNCH_NAME),
					'params' => array(
								$title,
								$text,
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Counter Number', BUNCH_NAME ),
									"name"			=>	"count_number",
									"description"	=>	esc_html__('Enter The Counter Number.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big AlphaBit', BUNCH_NAME ),
									"name"			=>	"big_alphabit",
									"description"	=>	esc_html__('Enter The Big AlphaBit.', BUNCH_NAME )
								),
								$number,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'team_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Contact Information
$options['bunch_contact_information']	=	array(
					'name' => esc_html__('Contact Information', BUNCH_NAME),
					'base' => 'bunch_contact_information',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Contact Information.', BUNCH_NAME),
					'params' => array(
								$bg_img,
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Counter Number', BUNCH_NAME ),
									"name"			=>	"count_number",
									"description"	=>	esc_html__('Enter The Counter Number.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Contact Form', BUNCH_NAME ),
									"name"			=>	"contact_form",
									"description"	=>	esc_html__('Enter The Contact Form.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Phone Number', BUNCH_NAME ),
									"name"			=>	"phone",
									"description"	=>	esc_html__('Enter The Phone Number.', BUNCH_NAME )
								),
							),
						);
//Feature Services
$options['bunch_feature_services']	=	array(
					'name' => esc_html__('Feature Services', BUNCH_NAME),
					'base' => 'bunch_feature_services',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Feature Services.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								$btn_title,
								$btn_link,
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Service', BUNCH_NAME ),
									'name' => 'service',
									'description' => esc_html__( 'Enter the Our Service.', BUNCH_NAME ),
									'params' => array(
												array(
													'type' => 'icon_picker',
													'label' => esc_html__( 'Icon', BUNCH_NAME ),
													'name' => 'icons',
													'description' => esc_html__( 'Enter The Icon.', BUNCH_NAME ),
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"ser_title",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Text', BUNCH_NAME ),
													"name"			=>	"ser_text",
													"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('External Link', BUNCH_NAME ),
													"name"			=>	"ext_link",
													"description"	=>	esc_html__('Enter The External Link.', BUNCH_NAME )
												),
											),
										),
									),
								);
//Case Study 2
$options['bunch_case_study_2']	=	array(
					'name' => esc_html__('Case Study 2', BUNCH_NAME),
					'base' => 'bunch_case_study_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Case Study 2.', BUNCH_NAME),
					'params' => array(
								$bg_img,
								$sub_title,
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Cases', BUNCH_NAME ),
									'name' => 'case',
									'description' => esc_html__( 'Enter the Our Cases.', BUNCH_NAME ),
									'params' => array(
												array(
													'type' => 'icon_picker',
													'label' => esc_html__( 'Icon', BUNCH_NAME ),
													'name' => 'icons',
													'description' => esc_html__( 'Enter The Icon.', BUNCH_NAME ),
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Value', BUNCH_NAME ),
													"name"			=>	"value",
													"description"	=>	esc_html__('Enter The Value.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
											),
										),
										array(
											"type"			=>	"select",
											"label"			=>	esc_html__("Border Style", BUNCH_NAME),
											"name"			=>	"title_style",
											'options'		=>	(array('one'=>esc_html__('Blue', BUNCH_NAME),'two'=>esc_html__('Red', BUNCH_NAME) ) ),			
											"description"	=>	esc_html__("Enter the Border Style.", BUNCH_NAME)
										),
									),
								);
//Our Services 4
$options['bunch_our_services_4']	=	array(
					'name' => esc_html__('Our Services 4', BUNCH_NAME),
					'base' => 'bunch_our_services_4',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Services 4.', BUNCH_NAME),
					'params' => array(
								$title,
								$text,
								$btn_title,
								$btn_link,
								$number,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'services_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Our Testimonials 4
$options['bunch_our_testimonials_4']	=	array(
					'name' => esc_html__('Our Testimonials 4', BUNCH_NAME),
					'base' => 'bunch_our_testimonials_4',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Testimonials 4.', BUNCH_NAME),
					'params' => array(
								$title,
								$text,
								$number,
								$text_limit,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'testimonials_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Our Partners 3
$options['bunch_our_partners_3']	=	array(
					'name' => esc_html__('Our Partners 3', BUNCH_NAME),
					'base' => 'bunch_our_partners_3',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Partners 3.', BUNCH_NAME),
					'params' => array(
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Sponsors', BUNCH_NAME ),
									'name' => 'sponsors',
									'description' => esc_html__( 'Enter the Our Sponsors.', BUNCH_NAME ),
									'params' => array(
												array(
													"type"			=>	"attach_image_url",
													"label"			=>	esc_html__('Sponsors Images', BUNCH_NAME ),
													"name"			=>	"img",
													'admin_label' 	=> 	false,
													"description"	=>	esc_html__('Choose Sponsors images Url.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('External Link', BUNCH_NAME ),
													"name"			=>	"ext_url",
													"description"	=>	esc_html__('Enter The External Link.', BUNCH_NAME )
												),
											),
										),
									),
								);
//Recent Projects
$options['bunch_recent_projects'] = array(
						'name' => esc_html__('Recent Projects', BUNCH_NAME),
						'base' => 'bunch_recent_projects',
						'class' => '',
						'category' => esc_html__('Oviedo', BUNCH_NAME),
						'icon' => 'fa-briefcase',
						'description' => esc_html__('Show Recent Projects.', BUNCH_NAME),
						'params' => array(
									esc_html__( 'Recent Project', BUNCH_NAME ) => array(
										$title,
										$text,
										array(
											 'type' => 'group',
											 'label' => esc_html__( 'FInancial Advise', BUNCH_NAME ),
											 'name' => 'slider',
											 'description' => esc_html__( 'Enter FInancial Advise.', BUNCH_NAME ),
											 'params' => array(
													array(
														 'type' => 'text',
														 'label' => esc_html__( 'Title', BUNCH_NAME ),
														 'name' => 'title1',
														 'description' => esc_html__( 'Enter Title of Menu Type .', BUNCH_NAME ),
													),
													array(
														 'type' => 'textarea',
														 'label' => esc_html__( 'Text', BUNCH_NAME ),
														 'name' => 'text1',
														 'description' => esc_html__( 'Enter Text .', BUNCH_NAME ),
													),
													$btn_title,
													$btn_link,
											 ),
										),//Group End
									),
									esc_html__( 'Our Projects', BUNCH_NAME ) => array(
										//Group Start
										array(
											 'type' => 'group',
											 'label' => esc_html__( 'Project Tab', BUNCH_NAME ),
											 'name' => 'product_tabs',
											 'description' => esc_html__( 'Enter Project Tab Info.', BUNCH_NAME ),
											 'params' => array(
													array(
														 'type' => 'text',
														 'label' => esc_html__( 'Title', BUNCH_NAME ),
														 'name' => 'tab_title',
														 'description' => esc_html__( 'Enter Title of Menu Type .', BUNCH_NAME ),
													),
													$number,
													array(
														"type" => "dropdown",
														"label" => __( 'Category', BUNCH_NAME),
														"name" => "cat",
														"options" =>  bunch_get_categories(array( 'taxonomy' => 'projects_category'), true),
														"description" => __( 'Choose Category.', BUNCH_NAME)
													),
													$order,
													$orderby,
													
											 ),
										),//Group End
									),
								),
							);
//Our Experience
$options['bunch_our_experience']	=	array(
					'name' => esc_html__('Our Experience', BUNCH_NAME),
					'base' => 'bunch_our_experience',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Experience.', BUNCH_NAME),
					'params' => array(
								$title,
								$text,
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Service', BUNCH_NAME ),
									'name' => 'service',
									'description' => esc_html__( 'Enter the Our Service.', BUNCH_NAME ),
									'params' => array(
												array(
													'type' => 'icon_picker',
													'label' => esc_html__( 'Icon', BUNCH_NAME ),
													'name' => 'icons',
													'description' => esc_html__( 'Enter The Icon.', BUNCH_NAME ),
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Text', BUNCH_NAME ),
													"name"			=>	"text1",
													"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('External Link', BUNCH_NAME ),
													"name"			=>	"ext_url",
													"description"	=>	esc_html__('Enter The External Link.', BUNCH_NAME )
												),
											),
										),
										array(
											"type"			=>	"checkbox",
											"label"			=>	esc_html__('Style Two', BUNCH_NAME ),
											"name"			=>	"style_two",
											'options' => array(
												'option_1' => 'Style Two',
											),
											"description"	=>	esc_html__('Choose whether you want to show The Background color.', BUNCH_NAME  )
										),
									),
								);
//Contact Information 2
$options['bunch_contact_information_2']	=	array(
					'name' => esc_html__('Contact Information 2', BUNCH_NAME),
					'base' => 'bunch_contact_information_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Contact Information 2.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Contact Form', BUNCH_NAME ),
									"name"			=>	"contact_form",
									"description"	=>	esc_html__('Enter The Contact Form.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Upper Title', BUNCH_NAME ),
									"name"			=>	"upper_title",
									"description"	=>	esc_html__('Enter The Upper Title.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Phone Number', BUNCH_NAME ),
									"name"			=>	"phone",
									"description"	=>	esc_html__('Enter The Phone Number.', BUNCH_NAME )
								),
								array(
									"type"			=>	"select",
									"label"			=>	esc_html__("Border Style", BUNCH_NAME),
									"name"			=>	"title_style",
									'options'		=>	(array('one'=>esc_html__('Blue', BUNCH_NAME),'two'=>esc_html__('Red', BUNCH_NAME),'three'=>esc_html__('Green', BUNCH_NAME) ) ),			
									"description"	=>	esc_html__("Enter the Border Style.", BUNCH_NAME)
								),
							),
						);
//Find Our Location 3
$options['bunch_find_our_location_3']	=	array(
					'name' => esc_html__('Find Our Location 3', BUNCH_NAME),
					'base' => 'bunch_find_our_location_3',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Find Our Location 3.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"attach_image_url",
									"label"			=>	esc_html__('Background Map Images', BUNCH_NAME ),
									"name"			=>	"map_img",
									'admin_label' 	=> 	false,
									"description"	=>	esc_html__('Choose Background Map images Url.', BUNCH_NAME )
								),
								array(
									"type"			=>	"attach_image_url",
									"label"			=>	esc_html__('Marker Images', BUNCH_NAME ),
									"name"			=>	"icon_img",
									'admin_label' 	=> 	false,
									"description"	=>	esc_html__('Choose Marker images Url.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Mark Title', BUNCH_NAME ),
									"name"			=>	"mark_title",
									"description"	=>	esc_html__('Enter The Mark Title.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Mark Text', BUNCH_NAME ),
									"name"			=>	"mark_text",
									"description"	=>	esc_html__('Enter The Mark Text.', BUNCH_NAME )
								),
							),
						);
//Marketing Expert
$options['bunch_marketing_expert']	=	array(
					'name' => esc_html__('Marketing Expert', BUNCH_NAME),
					'base' => 'bunch_marketing_expert',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Marketing Expert.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Service', BUNCH_NAME ),
									'name' => 'service',
									'description' => esc_html__( 'Enter the Our Service.', BUNCH_NAME ),
									'params' => array(
												array(
													'type' => 'icon_picker',
													'label' => esc_html__( 'Icon', BUNCH_NAME ),
													'name' => 'icons',
													'description' => esc_html__( 'Enter The Icon.', BUNCH_NAME ),
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"ser_title",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Text', BUNCH_NAME ),
													"name"			=>	"ser_text",
													"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('External Link', BUNCH_NAME ),
													"name"			=>	"ext_url",
													"description"	=>	esc_html__('Enter The External Link.', BUNCH_NAME )
												),
											),
										),
									),
								);
//Our Services 5
$options['bunch_our_services_5']	=	array(
					'name' => esc_html__('Our Services 5', BUNCH_NAME),
					'base' => 'bunch_our_services_5',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Services 5.', BUNCH_NAME),
					'params' => array(
								$title,
								$text,
								$btn_title,
								$btn_link,
								$number,
								$text_limit,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'services_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Recent Work
$options['bunch_recent_work']	=	array(
					'name' => esc_html__('Recent Work', BUNCH_NAME),
					'base' => 'bunch_recent_work',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Recent Work.', BUNCH_NAME),
					'params' => array(
								$title,
								$text,
								$btn_title,
								$btn_link,
								$number,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'projects_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Feature Services 2
$options['bunch_feature_services_2']	=	array(
					'name' => esc_html__('Feature Services 2', BUNCH_NAME),
					'base' => 'bunch_feature_services_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Feature Services 2.', BUNCH_NAME),
					'params' => array(
								$bg_img,
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alhpabit', BUNCH_NAME ),
									"name"			=>	"big_alhpabit",
									"description"	=>	esc_html__('Enter The Big Alhpabit.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Service', BUNCH_NAME ),
									'name' => 'service',
									'description' => esc_html__( 'Enter the Our Service.', BUNCH_NAME ),
									'params' => array(
												array(
													'type' => 'icon_picker',
													'label' => esc_html__( 'Icon', BUNCH_NAME ),
													'name' => 'icons',
													'description' => esc_html__( 'Enter The Icon.', BUNCH_NAME ),
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Designation', BUNCH_NAME ),
													"name"			=>	"designation",
													"description"	=>	esc_html__('Enter The Designation.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('External Link', BUNCH_NAME ),
													"name"			=>	"ext_url",
													"description"	=>	esc_html__('Enter The External Link.', BUNCH_NAME )
												),
											),
										),
									),
								);
//About Us 2
$options['bunch_about_us_2']	=	array(
					'name' => esc_html__('About Us 2', BUNCH_NAME),
					'base' => 'bunch_about_us_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The About Us 2.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"attach_image_url",
									"label"			=>	esc_html__('About Images', BUNCH_NAME ),
									"name"			=>	"about_img",
									'admin_label' 	=> 	false,
									"description"	=>	esc_html__('Choose About images Url.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								$sub_title,
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								$btn_title,
								$btn_link,
							),
						);
//Recent Work 2
$options['bunch_recent_work_2']	=	array(
					'name' => esc_html__('Recent Work 2', BUNCH_NAME),
					'base' => 'bunch_recent_work_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Recent Work 2.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								$title,
								$text,
								$btn_title,
								$btn_link,
								$number,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'projects_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Pricing Plan 2
$options['bunch_pricing_plan_2']	=	array(
					'name' => esc_html__('Pricing Plan 2', BUNCH_NAME),
					'base' => 'bunch_pricing_plan_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Pricing Plan 2.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								$sub_title,
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								array(
									'type' => 'group',
									'label' => esc_html__( 'Pricing Table', BUNCH_NAME ),
									'name' => 'table',
									'description' => esc_html__( 'Enter the Pricing Table.', BUNCH_NAME ),
									'params' => array(
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Button Description', BUNCH_NAME ),
													"name"			=>	"button_des",
													"description"	=>	esc_html__('Enter The Button Description.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Duration', BUNCH_NAME ),
													"name"			=>	"duration",
													"description"	=>	esc_html__('Enter The Duration.', BUNCH_NAME )
												),
												array(
													"type"			=>	"editor",
													"label"			=>	esc_html__('Price', BUNCH_NAME ),
													"name"			=>	"price",
													"description"	=>	esc_html__('Enter The Price.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Feature List', BUNCH_NAME ),
													"name"			=>	"feature_str",
													"description"	=>	esc_html__('Enter The Feature List.', BUNCH_NAME )
												),
												$btn_title,
												$btn_link,
											),
										),
									),
								);
//Our Testimonials 5
$options['bunch_our_testimonials_5']	=	array(
					'name' => esc_html__('Our Testimonials 5', BUNCH_NAME),
					'base' => 'bunch_our_testimonials_5',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Testimonials 5.', BUNCH_NAME),
					'params' => array(
								$title,
								$text,
								$number,
								$text_limit,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'testimonials_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
								array(
									"type"			=>	"attach_image_url",
									"label"			=>	esc_html__('Images', BUNCH_NAME ),
									"name"			=>	"image",
									'admin_label' 	=> 	false,
									"description"	=>	esc_html__('Choose images Url.', BUNCH_NAME )
								),
							),
						);
//Our Experience 2
$options['bunch_our_experience_2']	=	array(
					'name' => esc_html__('Our Experience 2', BUNCH_NAME),
					'base' => 'bunch_our_experience_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Experience 2.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Background Title', BUNCH_NAME ),
									"name"			=>	"bg_title",
									"description"	=>	esc_html__('Enter The Background Title.', BUNCH_NAME )
								),
								$title,
								$text,
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Service', BUNCH_NAME ),
									'name' => 'service',
									'description' => esc_html__( 'Enter the Our Service.', BUNCH_NAME ),
									'params' => array(
												array(
													'type' => 'icon_picker',
													'label' => esc_html__( 'Icon', BUNCH_NAME ),
													'name' => 'icons',
													'description' => esc_html__( 'Enter The Icon.', BUNCH_NAME ),
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Text', BUNCH_NAME ),
													"name"			=>	"text1",
													"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('External Link', BUNCH_NAME ),
													"name"			=>	"ext_url",
													"description"	=>	esc_html__('Enter The External Link.', BUNCH_NAME )
												),
											),
										),
									),
								);
//About and History
$options['bunch_about_and_history']	=	array(
					'name' => esc_html__('About and History', BUNCH_NAME),
					'base' => 'bunch_about_and_history',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The About and History.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabit', BUNCH_NAME ),
									"name"			=>	"big_alphabit",
									"description"	=>	esc_html__('Enter The Big AlphaBit.', BUNCH_NAME )
								),
								$title,
								$text,
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Description', BUNCH_NAME ),
									"name"			=>	"description",
									"description"	=>	esc_html__('Enter The Description.', BUNCH_NAME )
								),
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Service', BUNCH_NAME ),
									'name' => 'service',
									'description' => esc_html__( 'Enter the Our Service.', BUNCH_NAME ),
									'params' => array(
												$sub_title,
												array(
													"type"			=>	"attach_image_url",
													"label"			=>	esc_html__('Images', BUNCH_NAME ),
													"name"			=>	"img",
													'admin_label' 	=> 	false,
													"description"	=>	esc_html__('Choose images Url.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Text', BUNCH_NAME ),
													"name"			=>	"text1",
													"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
												),
											),
										),
									),
								);
//Funfacts 2
$options['bunch_funfacts_2']	=	array(
					'name' => esc_html__('Funfacts 2', BUNCH_NAME),
					'base' => 'bunch_funfacts_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Funfacts 2.', BUNCH_NAME),
					'params' => array(
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Funfact', BUNCH_NAME ),
									'name' => 'funfact',
									'description' => esc_html__( 'Enter the Our Funfact.', BUNCH_NAME ),
									'params' => array(
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Counter Start', BUNCH_NAME ),
													"name"			=>	"counter_start",
													"description"	=>	esc_html__('Enter The Counter Start.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Counter Stop', BUNCH_NAME ),
													"name"			=>	"counter_stop",
													"description"	=>	esc_html__('Enter The Counter Stop.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Plus Sign', BUNCH_NAME ),
													"name"			=>	"plus_icon",
													"description"	=>	esc_html__('Enter The Plus Sign.', BUNCH_NAME )
												),
											),
										),
									),
								);
//Our Faqs
$options['bunch_our_faqs']	=	array(
					'name' => esc_html__('Our Faqs', BUNCH_NAME),
					'base' => 'bunch_our_faqs',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Faqs.', BUNCH_NAME),
					'params' => array(
								$title,
								$text,
								$number,
								$text_limit,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'faqs_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Form Title', BUNCH_NAME ),
									"name"			=>	"form_title",
									"description"	=>	esc_html__('Enter The Form Title.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Contact Form', BUNCH_NAME ),
									"name"			=>	"contact_form",
									"description"	=>	esc_html__('Enter The Contact Form.', BUNCH_NAME )
								),
							),
						);
//Team Style One
$options['bunch_team_style_one']	=	array(
					'name' => esc_html__('Team Style One', BUNCH_NAME),
					'base' => 'bunch_team_style_one',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Team Style One.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								$number,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'team_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Team Style Two
$options['bunch_team_style_two']	=	array(
					'name' => esc_html__('Team Style Two', BUNCH_NAME),
					'base' => 'bunch_team_style_two',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Team Style Two.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								$number,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'team_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Team Style Three
$options['bunch_team_style_three']	=	array(
					'name' => esc_html__('Team Style Three', BUNCH_NAME),
					'base' => 'bunch_team_style_three',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Team Style Three.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								$number,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'team_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Team Style Four
$options['bunch_team_style_four']	=	array(
					'name' => esc_html__('Team Style Four', BUNCH_NAME),
					'base' => 'bunch_team_style_four',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Team Style Four.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								$number,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'team_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Pricing V1
$options['bunch_pricing_v1']	=	array(
					'name' => esc_html__('Pricing V1', BUNCH_NAME),
					'base' => 'bunch_pricing_v1',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Pricing V1.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Table', BUNCH_NAME ),
									'name' => 'table',
									'description' => esc_html__( 'Enter the Our Table.', BUNCH_NAME ),
									'params' => array(
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Text', BUNCH_NAME ),
													"name"			=>	"text1",
													"description"	=>	esc_html__('Enter The Plus Sign.', BUNCH_NAME )
												),
												array(
													"type"			=>	"editor",
													"label"			=>	esc_html__('Price', BUNCH_NAME ),
													"name"			=>	"price",
													"description"	=>	esc_html__('Enter The Price.', BUNCH_NAME )
												),
												$btn_title,
												$btn_link,
											),
										),
									),
								);
//Pricing V2
$options['bunch_pricing_v2']	=	array(
					'name' => esc_html__('Pricing V2', BUNCH_NAME),
					'base' => 'bunch_pricing_v2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Pricing V2.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Table', BUNCH_NAME ),
									'name' => 'table',
									'description' => esc_html__( 'Enter the Our Table.', BUNCH_NAME ),
									'params' => array(
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Duration', BUNCH_NAME ),
													"name"			=>	"duration",
													"description"	=>	esc_html__('Enter The Duration.', BUNCH_NAME )
												),
												array(
													"type"			=>	"editor",
													"label"			=>	esc_html__('Price', BUNCH_NAME ),
													"name"			=>	"price",
													"description"	=>	esc_html__('Enter The Price.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Feature List', BUNCH_NAME ),
													"name"			=>	"feature_str",
													"description"	=>	esc_html__('Enter The Feature List.', BUNCH_NAME )
												),
												$btn_title,
												$btn_link,
											),
										),
									),
								);
//Pricing V3
$options['bunch_pricing_v3']	=	array(
					'name' => esc_html__('Pricing V3', BUNCH_NAME),
					'base' => 'bunch_pricing_v3',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Pricing V3.', BUNCH_NAME),
					'params' => array(
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Table', BUNCH_NAME ),
									'name' => 'table',
									'description' => esc_html__( 'Enter the Our Table.', BUNCH_NAME ),
									'params' => array(
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Duration', BUNCH_NAME ),
													"name"			=>	"duration",
													"description"	=>	esc_html__('Enter The Duration.', BUNCH_NAME )
												),
												array(
													"type"			=>	"editor",
													"label"			=>	esc_html__('Price', BUNCH_NAME ),
													"name"			=>	"price",
													"description"	=>	esc_html__('Enter The Price.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Feature List', BUNCH_NAME ),
													"name"			=>	"feature_str",
													"description"	=>	esc_html__('Enter The Feature List.', BUNCH_NAME )
												),
												$btn_title,
												$btn_link,
											),
										),
									),
								);
//Services V1
$options['bunch_services_v1']	=	array(
					'name' => esc_html__('Services V1', BUNCH_NAME),
					'base' => 'bunch_services_v1',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Services V1.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								$number,
								$text_limit,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'services_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Contact Information 3
$options['bunch_contact_information_3']	=	array(
					'name' => esc_html__('Contact Information 3', BUNCH_NAME),
					'base' => 'bunch_contact_information_3',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Contact Information 3.', BUNCH_NAME),
					'params' => array(
								$bg_img,
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Contact Form', BUNCH_NAME ),
									"name"			=>	"contact_form",
									"description"	=>	esc_html__('Enter The Contact Form.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Sub Title', BUNCH_NAME ),
									"name"			=>	"sub_title",
									"description"	=>	esc_html__('Enter The Sub Title.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Phone Number', BUNCH_NAME ),
									"name"			=>	"phone",
									"description"	=>	esc_html__('Enter The Phone Number.', BUNCH_NAME )
								),
							),
						);
//Services V2
$options['bunch_services_v2']	=	array(
					'name' => esc_html__('Services V2', BUNCH_NAME),
					'base' => 'bunch_services_v2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Services V2.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								$number,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'services_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Services V3
$options['bunch_services_v3']	=	array(
					'name' => esc_html__('Services V3', BUNCH_NAME),
					'base' => 'bunch_services_v3',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Services V3.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$text,
								$number,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'services_category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Our Projects
$options['bunch_our_projects']	=	array(
					'name' => esc_html__('Our Projects', BUNCH_NAME),
					'base' => 'bunch_our_projects',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Projects.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$number,
								array(
									"type" => "textfield",
									"label" => __('Excluded Categories ID', BUNCH_NAME ),
									"name" => "exclude_cats",
									"description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
								),
								$order,
								$orderby,
							),
						);
//Our Projects 2
$options['bunch_our_projects_2']	=	array(
					'name' => esc_html__('Our Projects 2', BUNCH_NAME),
					'base' => 'bunch_our_projects_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Our Projects 2.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$number,
								array(
									"type" => "textfield",
									"label" => __('Excluded Categories ID', BUNCH_NAME ),
									"name" => "exclude_cats",
									"description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
								),
								$order,
								$orderby,
							),
						);
//Projects Masonry
$options['bunch_projects_masonry']	=	array(
					'name' => esc_html__('Projects Masonry', BUNCH_NAME),
					'base' => 'bunch_projects_masonry',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Projects Masonry.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$number,
								array(
									"type" => "textfield",
									"label" => __('Excluded Categories ID', BUNCH_NAME ),
									"name" => "exclude_cats",
									"description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
								),
								$order,
								$orderby,
							),
						);
//Projects FullWidth
$options['bunch_project_fullwidth']	=	array(
					'name' => esc_html__('Projects FullWidth', BUNCH_NAME),
					'base' => 'bunch_project_fullwidth',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Projects FullWidth.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$number,
								array(
									"type" => "textfield",
									"label" => __('Excluded Categories ID', BUNCH_NAME ),
									"name" => "exclude_cats",
									"description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
								),
								$order,
								$orderby,
							),
						);
//Projects FullWidth 2
$options['bunch_project_fullwidth_2']	=	array(
					'name' => esc_html__('Projects FullWidth 2', BUNCH_NAME),
					'base' => 'bunch_project_fullwidth_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Projects FullWidth 2.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$number,
								array(
									"type" => "textfield",
									"label" => __('Excluded Categories ID', BUNCH_NAME ),
									"name" => "exclude_cats",
									"description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
								),
								$order,
								$orderby,
							),
						);
//Projects FullWidth 3
$options['bunch_project_fullwidth_3']	=	array(
					'name' => esc_html__('Projects FullWidth 3', BUNCH_NAME),
					'base' => 'bunch_project_fullwidth_3',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Projects FullWidth 3.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$number,
								array(
									"type" => "textfield",
									"label" => __('Excluded Categories ID', BUNCH_NAME ),
									"name" => "exclude_cats",
									"description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
								),
								$order,
								$orderby,
							),
						);
//Projects FullWidth 4
$options['bunch_project_fullwidth_4']	=	array(
					'name' => esc_html__('Projects FullWidth 4', BUNCH_NAME),
					'base' => 'bunch_project_fullwidth_4',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Projects FullWidth 4.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Big Alphabet', BUNCH_NAME ),
									"name"			=>	"big_alphabet",
									"description"	=>	esc_html__('Enter The Big Alphabet.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								$number,
								array(
									"type" => "textfield",
									"label" => __('Excluded Categories ID', BUNCH_NAME ),
									"name" => "exclude_cats",
									"description" => __('Enter Excluded Categories ID seperated by commas(13,14).', BUNCH_NAME )
								),
								$order,
								$orderby,
							),
						);
//Project Single
$options['bunch_project_single']	=	array(
					'name' => esc_html__('Project Single', BUNCH_NAME),
					'base' => 'bunch_project_single',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Project Single.', BUNCH_NAME),
					'params' => array(
								esc_html__( 'General', BUNCH_NAME ) => array(
									array(
										"type"			=>	"attach_image_url",
										"label"			=>	esc_html__('Project Images', BUNCH_NAME ),
										"name"			=>	"image",
										'admin_label' 	=> 	false,
										"description"	=>	esc_html__('Choose Project Images Url.', BUNCH_NAME )
									),
									array(
										'type' => 'group',
										'label' => esc_html__( 'Project Information', BUNCH_NAME ),
										'name' => 'info',
										'description' => esc_html__( 'Enter the Project Information.', BUNCH_NAME ),
										'params' => array(
													array(
														"type"			=>	"text",
														"label"			=>	esc_html__('Title', BUNCH_NAME ),
														"name"			=>	"title",
														"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
													),
													array(
														"type"			=>	"text",
														"label"			=>	esc_html__('Text', BUNCH_NAME ),
														"name"			=>	"text",
														"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
													),
												),
											),
											$btn_title,
											$btn_link,
											array(
												"type"			=>	"editor",
												"label"			=>	esc_html__('Contents', BUNCH_NAME ),
												"name"			=>	"content_text",
												"description"	=>	esc_html__('Enter The Contents.', BUNCH_NAME )
											),
										),
										esc_html__( 'Our Project', BUNCH_NAME ) => array(
											array(
												"type"			=>	"textarea",
												"label"			=>	esc_html__('Sub Title', BUNCH_NAME ),
												"name"			=>	"sub_title",
												"description"	=>	esc_html__('Enter The Sub Title.', BUNCH_NAME )
											),
											array(
												"type"			=>	"textarea",
												"label"			=>	esc_html__('Text', BUNCH_NAME ),
												"name"			=>	"text1",
												"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
											),
											array(
												'type' => 'group',
												'label' => esc_html__( 'Project Information', BUNCH_NAME ),
												'name' => 'projects',
												'description' => esc_html__( 'Enter the Project Information.', BUNCH_NAME ),
												'params' => array(
															array(
																"type"			=>	"attach_image_url",
																"label"			=>	esc_html__('Project Image', BUNCH_NAME ),
																"name"			=>	"proj_img",
																'admin_label' 	=> 	false,
																"description"	=>	esc_html__('Choose Project Images Url.', BUNCH_NAME )
															),
															array(
																"type"			=>	"text",
																"label"			=>	esc_html__('External Link', BUNCH_NAME ),
																"name"			=>	"ext_url",
																"description"	=>	esc_html__('Enter The External Link.', BUNCH_NAME )
															),
														),
													),
												),
												esc_html__( 'Overview', BUNCH_NAME ) => array(
													array(
														'type' => 'group',
														'label' => esc_html__( 'Overview', BUNCH_NAME ),
														'name' => 'overviews',
														'description' => esc_html__( 'Enter the Overview.', BUNCH_NAME ),
														'params' => array(
																	array(
																		"type"			=>	"text",
																		"label"			=>	esc_html__('Title', BUNCH_NAME ),
																		"name"			=>	"title1",
																		"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
																	),
																	array(
																		"type"			=>	"textarea",
																		"label"			=>	esc_html__('Text', BUNCH_NAME ),
																		"name"			=>	"text2",
																		"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
																	),
																),
															),
														),
													),
												);
//Blog 2 Column
$options['bunch_blog_2_col']	=	array(
					'name' => esc_html__('Blog 2 Column', BUNCH_NAME),
					'base' => 'bunch_blog_2_col',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Blog 2 Column.', BUNCH_NAME),
					'params' => array(
								$number,
								$text_limit,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Blog 3 Column
$options['bunch_blog_3_col']	=	array(
					'name' => esc_html__('Blog 3 Column', BUNCH_NAME),
					'base' => 'bunch_blog_3_col',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Blog 3 Column.', BUNCH_NAME),
					'params' => array(
								$number,
								array(
									"type" => "dropdown",
									"label" => __( 'Category', BUNCH_NAME),
									"name" => "cat",
									"options" =>  bunch_get_categories(array( 'taxonomy' => 'category'), true),
									"description" => __( 'Choose Category.', BUNCH_NAME)
								),
								$order,
								$orderby,
							),
						);
//Contact Us
$options['bunch_contact_us']	=	array(
					'name' => esc_html__('Contact Us', BUNCH_NAME),
					'base' => 'bunch_contact_us',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Contact Us.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								array(
									'type' => 'group',
									'label' => esc_html__( 'Our Information', BUNCH_NAME ),
									'name' => 'information',
									'description' => esc_html__( 'Enter the Our Information.', BUNCH_NAME ),
									'params' => array(
												array(
													'type' => 'icon_picker',
													'label' => esc_html__( 'Icon', BUNCH_NAME ),
													'name' => 'icons',
													'description' => esc_html__( 'Enter The Icon.', BUNCH_NAME ),
												),
												array(
													"type"			=>	"text",
													"label"			=>	esc_html__('Title', BUNCH_NAME ),
													"name"			=>	"title1",
													"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
												),
												array(
													"type"			=>	"textarea",
													"label"			=>	esc_html__('Text', BUNCH_NAME ),
													"name"			=>	"text",
													"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
												),
												array(
													'name'        => 'style_two',
													'label'       => esc_html__( 'Social Icons', BUNCH_NAME ),
													'type'        => 'checkbox',
													'description' => esc_html__( 'Whether to show Social Icons or not', BUNCH_NAME ),
													'options'     => array( 'yes' => 'Yes, Please!' )
												),
											),
										),
									),
								);
//Google Map
$options['bunch_google_map']	=	array(
					'name' => esc_html__('Google Map', BUNCH_NAME),
					'base' => 'bunch_google_map',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Google Map.', BUNCH_NAME),
					'params' => array(
								esc_html__( 'Google Map', BUNCH_NAME ) => array(
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Latitude', BUNCH_NAME ),
										"name"			=>	"lat",
										"description"	=>	esc_html__('Enter The Latitude.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Longitude', BUNCH_NAME ),
										"name"			=>	"long",
										"description"	=>	esc_html__('Enter The Longitude.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Mark Title', BUNCH_NAME ),
										"name"			=>	"mark_title",
										"description"	=>	esc_html__('Enter The Mark Title.', BUNCH_NAME )
									),
									array(
										"type"			=>	"attach_image_url",
										"label"			=>	esc_html__('Marker Images', BUNCH_NAME ),
										"name"			=>	"img",
										'admin_label' 	=> 	false,
										"description"	=>	esc_html__('Choose Marker Images Url.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Mark Address', BUNCH_NAME ),
										"name"			=>	"mark_address",
										"description"	=>	esc_html__('Enter The Mark Address.', BUNCH_NAME )
									),
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Email', BUNCH_NAME ),
										"name"			=>	"email",
										"description"	=>	esc_html__('Enter The Email.', BUNCH_NAME )
									),
								),
								esc_html__( 'Contact Form', BUNCH_NAME ) => array(
									array(
										"type"			=>	"text",
										"label"			=>	esc_html__('Title', BUNCH_NAME ),
										"name"			=>	"form_title",
										"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
									),
									array(
										"type"			=>	"textarea",
										"label"			=>	esc_html__('Contact Form', BUNCH_NAME ),
										"name"			=>	"contact_form",
										"description"	=>	esc_html__('Enter The Contact Form.', BUNCH_NAME )
									),
								),
							),
						);
//Google Map 2
$options['bunch_google_map_2']	=	array(
					'name' => esc_html__('Google Map 2', BUNCH_NAME),
					'base' => 'bunch_google_map_2',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Google Map 2.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Latitude', BUNCH_NAME ),
									"name"			=>	"lat",
									"description"	=>	esc_html__('Enter The Latitude.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Longitude', BUNCH_NAME ),
									"name"			=>	"long",
									"description"	=>	esc_html__('Enter The Longitude.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Mark Title', BUNCH_NAME ),
									"name"			=>	"mark_title",
									"description"	=>	esc_html__('Enter The Mark Title.', BUNCH_NAME )
								),
								array(
									"type"			=>	"attach_image_url",
									"label"			=>	esc_html__('Marker Images', BUNCH_NAME ),
									"name"			=>	"img",
									'admin_label' 	=> 	false,
									"description"	=>	esc_html__('Choose Marker Images Url.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Mark Address', BUNCH_NAME ),
									"name"			=>	"mark_address",
									"description"	=>	esc_html__('Enter The Mark Address.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Email', BUNCH_NAME ),
									"name"			=>	"email",
									"description"	=>	esc_html__('Enter The Email.', BUNCH_NAME )
								),
							),
						);
//Contact Us Form
$options['bunch_contact_us_form']	=	array(
					'name' => esc_html__('Contact Us Form', BUNCH_NAME),
					'base' => 'bunch_contact_us_form',
					'class' => '',
					'category' => esc_html__('Oviedo', BUNCH_NAME),
					'icon' => 'fa-briefcase' ,
					'description' => esc_html__('Show The Contact Us Form.', BUNCH_NAME),
					'params' => array(
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Contact Form', BUNCH_NAME ),
									"name"			=>	"contact_form",
									"description"	=>	esc_html__('Enter The Contact Form.', BUNCH_NAME )
								),
								array(
									"type"			=>	"textarea",
									"label"			=>	esc_html__('Title', BUNCH_NAME ),
									"name"			=>	"title",
									"description"	=>	esc_html__('Enter The Title.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Text', BUNCH_NAME ),
									"name"			=>	"text",
									"description"	=>	esc_html__('Enter The Text.', BUNCH_NAME )
								),
								array(
									"type"			=>	"text",
									"label"			=>	esc_html__('Phone Number', BUNCH_NAME ),
									"name"			=>	"phone",
									"description"	=>	esc_html__('Enter The Phone Number.', BUNCH_NAME )
								),
								array(
									'name'        => 'style_two',
									'label'       => esc_html__( 'Social Icons', BUNCH_NAME ),
									'type'        => 'checkbox',
									'description' => esc_html__( 'Whether to show Social Icons or not', BUNCH_NAME ),
									'options'     => array( 'yes' => 'Yes, Please!' )
								),
							),
						);




return $options;