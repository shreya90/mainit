<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mainit');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('FS_METHOD', 'direct');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A:IrA{#-d}n#3(>-mZir<J@2`>.tGk%KG`Pdbg@B?4S6_spofLs]gg2Jg AO(Gjw');
define('SECURE_AUTH_KEY',  'p:VDqZ89L2. 9(:`MFCcFqm9jh)e)HVwl*yYhksA_p9Le]:xj%{4}]lxNS>!E#;L');
define('LOGGED_IN_KEY',    '.H/t.DT*sa>;- ,{xPjWv;nJHs^0o.Byw03liR 4}{aX3?3)_qHdkp7eF*C3vZag');
define('NONCE_KEY',        'vX<Di;+T!_z5Q^UFNhT_Sb8HVVYw*7eWf?wP<=XVf$J7|k=%SJG+0U2C%BD}x@Zy');
define('AUTH_SALT',        '7^_N{/v8U[pNv+?tB?;GM*=oDYB!H+WwDeg`REmd~8;bR/9JHN&uwEs>G0J,toJ(');
define('SECURE_AUTH_SALT', 'Xg2HD{OWe0|rd2n]C1}ty2drx7W-}Z>v#9qkQC! mQv=^v6{-|Jd+CbzKj,Rb#~@');
define('LOGGED_IN_SALT',   'W9jKGwMbyzoq#DM]UWUv:{(cGC:+z*eaB+^s#@Mm;VO*IE/i*zaBC{}vt1>$Rx(T');
define('NONCE_SALT',       'aZAW;{_Ko0{rLZ:L^.n*BA^n5>*}{z1;_s <Fg4X)ae!nZgt|%3xlw&9;JaKyQMD');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
